#include "neuron_system.h"
#include <iostream>
#include <cmath>

using namespace std;


/*
 * A virtual class for a neuron/other cell model.
 * To see how it can be used as an interface for a specific
 * model, look at for example the HodgkinHuxley class.
 * Models implementing this interface can be solved using
 * the methods in the accompanying solver-class.
 */
neuron_system::neuron_system()
{
}
