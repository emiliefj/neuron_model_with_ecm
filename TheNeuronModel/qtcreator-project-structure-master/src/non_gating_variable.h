#ifndef NON_GATING_VARIABLE_H
#define NON_GATING_VARIABLE_H
#include <cmath>
#include<vector>
#include <stdlib.h>
#include <iostream>


/*
 * A class for containing the varios non gating variables of a neuron model.
 */

class non_gating_variable
{
public:    
    non_gating_variable();
    non_gating_variable(double initial_value, double A,double B,double C, double x0, double x1, double theta, double k);

    double rhs(double myval,double Pval,double Qval, int myindex, double V);
    double H(double Q);
    void set_A(double val);
    double get_value();

    double x; // Value of the non-gating variable

private:
    double A,B,C; // alpha_x, gamma, beta_x
    double x0,x1;
    double theta;
    double k;

};

#endif // NON_GATING_VARIABLE_H
