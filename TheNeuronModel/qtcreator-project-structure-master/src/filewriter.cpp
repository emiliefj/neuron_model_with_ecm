#include "filewriter.h"


/*
 * Constructor setting up the system. Takes in the system to write from, a base for
 * the file name, as well as the chosen solver method, # of timesteps T, step size
 * dt, the frequency for writing to file, and a comment for the top of the file to
 * be written.
 */
filewriter::filewriter(neuron_system *sys,string base_name, string model, string method, int T, double dt, int freq, string comment)
{
    this->mysys = sys;

    this->dt = dt;
    this->T = T;
    this->writing_freq = freq;
    this->freq_count = freq;
    this->base_name = base_name;
    this->var = var;
    this->model = model;
    this->method = method;    // The solver method ("rk4", "rl", "fe")
    this->comment = comment;
    this->first = true;

    filename = base_name +"_"+ model +"_"+ method + "_T_"+ to_string(T) +"_dt_"+ to_string(dt) + ".txt";

    myfile.open(filename, ios::out);
    myfile << comment << "\n";
    myfile << (T/(freq+1)) << "\n";
    myfile << dt << "\n";
    //myfile << freq << "\n";
    myfile.close();

    cout<<"Writing to file: \""<<filename<<"\". "<<endl;

    first = true;
}


/*
 * Writes V, Q, Z, P, R, b, Ith and the timestep t to file, by appending this information
 * to the end of the furrent file filename.
 */
void filewriter::write(double t){

    V = mysys->V;
    Q = mysys->non_gating_variables[0].x;
    Z = mysys->non_gating_variables[1].x;
    P = mysys->non_gating_variables[2].x;
    R = mysys->non_gating_variables[3].x;
    b = mysys->b();
    Ith = mysys->Ith();

    if(freq_count==writing_freq){
        if(first){
            myfile.open(filename, ios::app); // open file and append value at the end
            myfile << "     V     " << "     Q    " << "     Z    "<<"     P    "<<"     R    "<<"     b    "<<"    Ith   "<<"     t    "<<"\n";
            this->first = false;
            myfile.close();
        }
        myfile.open(filename, ios::app); // open file and append value at the end
        myfile << to_string(V) << "  " <<to_string(Q) << "  " <<to_string(Z) << "  " <<to_string(P) << "  " <<to_string(R) << "  " <<to_string(b) << "  " <<to_string(Ith) << "  " <<to_string(t) <<"\n";
        myfile.close();
        freq_count=0;
    }
    else{
        freq_count++;
    }
}

/*
 * Writes the string line to file by appending it to the end of the file
 * filename.
 */
void filewriter::write_line(string line){
        myfile.open(filename, ios::app); // open file and append value at the end
        myfile << line << "\n";
        myfile.close();
}


