#ifndef GATING_VARIABLE_H
#define GATING_VARIABLE_H


#include<vector>

using namespace std;


/*
 * A class for containing the gating variables of a neuron model
 */
class gating_variable
{
public:

    // Methods:
    gating_variable();
    gating_variable(double init_value, double alpha_A, double alpha_B, double alpha_C, double alpha_D, double alpha_F, double beta_A, double beta_B, double beta_C, double beta_D, double beta_F);

    double alpha(double V);
    double beta(double V);
    double rhs(double value, double V);

    // Attributes:
    double x;
    double alpha_A, alpha_B, alpha_C, alpha_D, alpha_F;
    double beta_A, beta_B, beta_C, beta_D, beta_F;



};

#endif // GATING_VARIABLE_H
