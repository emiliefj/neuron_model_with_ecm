#ifndef SOLVER_H
#define SOLVER_H
#include"hodgkinhuxley.h"
#include"neuron_system.h"
#include"kazantsev.h"
#include"gating_variable.h"
#include"non_gating_variable.h"
#include"filewriter.h"
#include <iostream>
#include <stdlib.h>

using namespace std;

class solver
{
public:

    neuron_system* sys;
    bool spike;
    int spike_count; // for statistics
    double spike_duration;
    double average_spike_duration;
    string method;

    solver(neuron_system *sys);
    void explicit_RK4(double dt, int t);
    void rush_larsen(double dt, int t);
    void explicit_forward_euler(double dt, int t);
    void I_F_forward_Euler(double dt, int t);

    void solve(double dt, int t,int freq,string method, string bn, string model, string comment);
    void advance(int i, double dt, string method);
};

#endif // SOLVER_H
