#include "hodgkinhuxley.h"
#include <iostream>
#include <cmath>
#include <stdlib.h>

using namespace std;


/*
 * A class for the standard Hodgkin-Huxley model. The class implements the virtual
 * class Neuron_system.
 *
 * The Hodgkin-Huxley models the electrical activity and spike generation in a neuon as:
 *
 * C*dV/dt =-Imem - Isyn
 *
 * with Imem = I_Na + I_K + I_l
 *
 * where:
 *
 * I_Na = g_Na*m^3*h(V-V_Na)
 * I_K = g_K*n^4(V-V_K)
 * I_Na = g_l(V-V_l)
 *
 * The gating variables m,h,n are stored as instances of the gating_variable-class in
 * the gating_variables-vector, and follow the differential equation:
 *
 * dx/dt = a_x(1-x)-b_x*x
 *
 */
hodgkinhuxley::hodgkinhuxley()
{
    /* Constructor */
}

/*
 * Setting the values for the constants and variables
 * in the system.
 */
void hodgkinhuxley::set_initial_values(double dt){
    this->nr_of_gating_vars = 3;

    // Initial values for gating variables and voltage
    double m = 0.05;  // m
    double h = 0.6;   // h
    double n = 0.325; // n
    this->V = -64;//-54;   // V


    // The parameters as given in box 3.5, p 61, Sterrat
    // m:
    this->m_aa = -4;
    this->m_ab = -0.1;
    this->m_ac = -4;
    this->m_ad = -0.1;
    this->m_af = -1;

    this->m_ba = 4.0;
    this->m_bb = 0.0;
    this->m_bc = 65.0/18;
    this->m_bd = 1.0/18;
    this->m_bf = 0.0;

    // n:
    this->n_aa = -0.55;
    this->n_ab = -0.01;
    this->n_ac = -5.5;
    this->n_ad = -0.1;
    this->n_af = -1;

    this->n_ba = 0.125;
    this->n_bb = 0;
    this->n_bc = 0.8125;
    this->n_bd = 0.0125;
    this->n_bf = 0.0;

    // h:
    this->h_aa = 0.07;
    this->h_ab = 0.0;
    this->h_ac = 3.25;
    this->h_ad = 0.05;
    this->h_af = 0.0;

    this->h_ba = 1.0;
    this->h_bb = 0;
    this->h_bc = -3.5;
    this->h_bd = -0.1;
    this->h_bf = 1;

    this->g_Na = 120;   // mS/cm^2
    this->V_Na = 50;    //115.-65 mV
    this->g_K = 36;     // mS/cm^2
    this->V_K = -77;    //-12.-65 mV
    this->g_l = 0.3;    // mS/cm^2
    this->V_l = -54.4;  //10.613-65 mV
    this->Cm = 1;

    // Initializing the gating variables:
    this->gating_variables[0] = gating_variable(m,m_aa,m_ab,m_ac,m_ad,m_af,m_ba,m_bb,m_bc,m_bd,m_bf);  // m
    this->gating_variables[1] = gating_variable(h,h_aa,h_ab,h_ac,h_ad,h_af,h_ba,h_bb,h_bc,h_bd,h_bf);  // h
    this->gating_variables[2] = gating_variable(n,n_aa,n_ab,n_ac,n_ad,n_af,n_ba,n_bb,n_bc,n_bd,n_bf);  // n

    this->nr_of_gating_vars = 3;
    this->nr_of_non_gating_vars = 0;

    // For the synaptic input:
    this->syn_start = 20; // ms
    this->syn_end = 22;
    this->syn_val = 20;

}


/*
 * Returns the value for the sodium current,
 * using the current values for the gating variables
 * m and h, and the current membrane voltage V.
 */
double hodgkinhuxley::I_Na(){
    double m = gating_variables[0].x;
    double h = gating_variables[1].x;

    return g_Na*(m*m*m)*h*(V-V_Na);
}

/*
 * Returns the value for the potassium current,
 * using the current values for the gating variable
 * n, and the current membrane voltage V.
 */
double hodgkinhuxley::I_K(){
    double n = gating_variables[2].x;

    return g_K*(n*n*n*n)*(V-V_K);
}

/*
 * Returns the value for the leak current,
 * using the current value for the membrane
 * voltage V.
 */
double hodgkinhuxley::I_l(){
    return g_l*(V-V_l);
}

/*
 * Returns the total membrane current.
 */
double hodgkinhuxley::Imem(){
    return I_Na() + I_K() + I_l();
}

/*
 * Returns the synaptic input to the neuron, currently in the
 * form of a pulse of size syn_val lasting from t=syn_start
 * until t=syn_end.
 * If you want a different form of input you can change this
 * implementation, or perhaps better, create a new class that
 * inherits from this one, e.g. hodgkinhuxley_inputtype, and
 * inplement its Isyn()-function to better fit your needs.
 */
double hodgkinhuxley::Isyn(int t,const double dt){
    return (t*dt <= syn_end && t*dt >= syn_start ? syn_val : 0.);
}

/*
 * Returns the RHS of the voltage equation in the
 * Hodgkin-Huxley model. Takes in the current timestep t
 * and the step size dt, to pass it on to Isyn() which
 * uses it to calculate the current simulation time.
 */
double hodgkinhuxley::dV_dt(const double dt, const int t){
    return -(Imem()-Isyn(t,dt))/(Cm);
}

/*
 * Returns the scaling of the snaptic input, currently not in use in
 * this model.
 */
double hodgkinhuxley::b(){
    return 0;
}

/*
 * Returns the value of an applied current, currently not in use
 * in this model.
 */
double hodgkinhuxley::Ith(){
    return 0;
}

/*
 * Sets the values of the conductances for the membrane currents
 */
void hodgkinhuxley::set_conductances(double Na,double K, double L){
    g_Na = Na;
    g_K = K;
    g_l = L;
}

/*
 * Sets the values of the equilibrium potentials for the membrane currents
 */
void hodgkinhuxley::set_equilibrium_potentials(double Na,double K, double L){
    V_Na = Na;
    V_K = K;
    V_l = L;
}

/*
 * Sets the values of the values for the gating variables included in
 * this model.
 */
void hodgkinhuxley::set_gating_variable_values(double m,double n, double h){
    gating_variables[0].x = m;
    gating_variables[1].x = h;
    gating_variables[2].x = n;
}

/*
 * Sets the value for the membrane capacitance.
 */
void hodgkinhuxley::set_C(double val){
    Cm = val;
}

/*
 * Sets the timing and value of the synaptic input-pulse.
 */
void hodgkinhuxley::set_synaptic_input(double start, double end, double val){
    syn_start = start;
    syn_end = end;
    syn_val = val;
}




