#include "gating_variable.h"
#include <iostream>
#include <cmath>

using namespace std;



/*
 * A class for containing the gating variables of a neuron model.
 * The gating variables are variables which control the "openness" of
 * the ion channels in the cell membrane.
 *
 * The gating variable x obeys the differential equation:
 *
 * dx/dt = a_x*(1-x)-b_x*x
 *
 * where
 *
 * a_x = (alpha_A+alpha_B*V)/(exp(alpha_C+alpha_D*V)+alpha_F),
 *
 * and
 *
 * b_x = (beta_A+beta_B*V)/(exp(beta_C+beta_D*V)+beta_F).
 */

gating_variable::gating_variable(){

}

/* Constructor where the various constants are set */
gating_variable::gating_variable(double init_value, double alpha_A, double alpha_B, double alpha_C, double alpha_D, double alpha_F, double beta_A, double beta_B, double beta_C, double beta_D, double beta_F)
{
    // Setting the initial value of the variable:
    this->x = init_value;

    // Setting the constants:
    this->alpha_A = alpha_A;
    this->alpha_B = alpha_B;
    this->alpha_C = alpha_C;
    this->alpha_D = alpha_D;
    this->alpha_F = alpha_F;
    this->beta_A = beta_A;
    this->beta_B = beta_B;
    this->beta_C = beta_C;
    this->beta_D = beta_D;
    this->beta_F = beta_F;
}

/* returns the current value for a_x */
double gating_variable::alpha(double V){

    return (alpha_A+alpha_B*V)/(exp(alpha_C+alpha_D*V)+alpha_F);
}

/* returns the current value for b_x */
double gating_variable::beta(double V){

    return (beta_A+beta_B*V)/(exp(beta_C+beta_D*V)+beta_F);
}


/*
 * Calculates and returns the right hand side of the
 * differential equation describing the dynamics of the
 * gating variable, using the current value o the gating
 * variable and the current value of the membrane potential.
 */
double gating_variable::rhs(double value,double V){

    return alpha(V)*(1-value)-beta(V)*value;
}
