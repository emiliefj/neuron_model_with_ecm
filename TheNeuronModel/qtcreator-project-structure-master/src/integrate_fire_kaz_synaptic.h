#ifndef INTEGRATE_FIRE_KAZ_SYNAPTIC_H
#define INTEGRATE_FIRE_KAZ_SYNAPTIC_H
#include"kazantsev.h"

class integrate_fire_kaz_synaptic : public kazantsev
{

public:
    integrate_fire_kaz_synaptic();
    //double V;
    double Vm;
    double Rm;
    double thresh;

    void set_initial_values(double dt);
    double dV_dt(double dt, int t);
    double I_RC();

    double check_threshold();

};

#endif // INTEGRATE_FIRE_KAZ_SYNAPTIC_H
