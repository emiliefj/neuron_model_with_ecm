#include "kazantsev_pulse.h"


/*
 * This class is the same as the kazantsev-class, except that
 * the synaptic input is now in the form of a simple pulse.
 */
kazantsev_pulse::kazantsev_pulse()
{
}

/*
 * Returns the synaptic input in the form of a pulse. If the
 * amplitude and start and end times of the pulse is not given,
 * a default pulse is used.
 */
double kazantsev_pulse::Isyn(int t, double dt){
    if(Isyn_amp){
        return (t*dt <= Isyn_end && t*dt >= Isyn_start ? Isyn_amp : 0.);
    }
    //cout<<t*dt<<endl;
    return (t*dt <= 3. && t*dt >= 1. ? 17 : 0.);
}

/* Sets the amplitude of the input pulse to val */
void kazantsev_pulse::set_Isyn_amplitude(double val){
    this->Isyn_amp = val;
}

/* Sets the start-time and end-time of the input pulse to
 * tmin and tmax, respectively, given in ms.
 */
void kazantsev_pulse::set_Isyn_time(double tmin,double tmax){
    this->Isyn_start = tmin;
    this->Isyn_end = tmax;
}
