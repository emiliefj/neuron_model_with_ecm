#ifndef HODGKINHUXLEY_H
#define HODGKINHUXLEY_H
#include"neuron_system.h"
#include"gating_variable.h"

using namespace std;


/*
 * A class for the standard Hodgkin-Huxley model. The class implements the virtual
 * class neuron_system.
 */
class hodgkinhuxley : public neuron_system
{
public:
    hodgkinhuxley();

    // Parameters:
    double g_Na;
    double V_Na;
    double g_K;
    double V_K;
    double g_l;
    double V_l;
    double Cm;

    // for the synaptic input:
    double syn_start;
    double syn_end;
    double syn_val;


    // Model methods:
    void set_initial_values(double dt);

    double I_Na();
    double I_K();
    double I_l();
    double Imem();
    double b();
    double Ith();
    double Isyn(int t,const double dt);

    double dV_dt(const double dt, const int t);

    // Varios set-functions:
    void set_conductances(double Na,double K, double L);
    void set_equilibrium_potentials(double Na,double K, double L);
    void set_gating_variable_values(double m,double n, double h);
    void set_C(double val);
    void set_synaptic_input(double start,double end,double val);

};

#endif // HODGKINHUXLEY_H
