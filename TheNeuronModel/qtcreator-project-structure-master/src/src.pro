include(../defaults.pri)

CONFIG   += console
CONFIG   -= app_bundle
CONFIG   -= qt
CONFIG   += c++11

TEMPLATE = lib

TARGET = myapp

SOURCES += main.cpp \
    hodgkinhuxley.cpp \
    solver.cpp \
    gating_variable.cpp \
    kazantsev.cpp \
    non_gating_variable.cpp \
    filewriter.cpp \
    neuron_system.cpp \
    kazantsev_pulse.cpp \
    integrate_fire_kazantsev.cpp \
    integrate_fire_kaz_synaptic.cpp

HEADERS += \
    hodgkinhuxley.h \
    solver.h \
    gating_variable.h \
    kazantsev.h \
    non_gating_variable.h \
    filewriter.h \
    neuron_system.h \
    kazantsev_pulse.h \
    integrate_fire_kazantsev.h \
    integrate_fire_kaz_synaptic.h
