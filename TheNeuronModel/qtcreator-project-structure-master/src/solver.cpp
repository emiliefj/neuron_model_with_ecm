#include "solver.h"
#include <iostream>
#include <cmath>

using namespace std;

solver::solver(neuron_system *sys)
{
    this->sys = sys;
    spike_count = 0;
    spike = 0;
}


// A function for computing a forward step in the Hodgkin-Huxley model
// using the standard Runge-Kutta method. BUGFIX: RK4 not currently
// working ideally for models with Poisson input.
void solver::explicit_RK4(double dt, int t){
    // V:
    double Vk1,Vk2,Vk3,Vk4;
    double Vn = sys->V;

    // Gating variables:
    double k1_gv[sys->nr_of_gating_vars];
    double k2_gv[sys->nr_of_gating_vars];
    double k3_gv[sys->nr_of_gating_vars];
    double k4_gv[sys->nr_of_gating_vars];
    double gv_n[sys->nr_of_gating_vars];
    for(int i=0;i<sys->nr_of_gating_vars;i++){
        gv_n[i] = sys->gating_variables[i].x;
    }

    // Non-gating variables:
    double k1_ngv[sys->nr_of_non_gating_vars];
    double k2_ngv[sys->nr_of_non_gating_vars];
    double k3_ngv[sys->nr_of_non_gating_vars];
    double k4_ngv[sys->nr_of_non_gating_vars];
    double ngv_n[sys->nr_of_non_gating_vars];
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        ngv_n[i] = sys->non_gating_variables[i].x;
    }

    // // // // // //
    // finding k1  //
    // // // // // //

    // For the gating variables:
    for(int i=0;i<sys->nr_of_gating_vars;i++){
        k1_gv[i] = sys->gating_variables[i].rhs(sys->gating_variables[i].x,sys->V);
        sys->gating_variables[i].x = sys->gating_variables[i].x+dt*0.5*k1_gv[i];
    }

    // For the non-gating variables:
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        double val = sys->non_gating_variables[i].x;
        k1_ngv[i] = sys->non_gating_variables[i].rhs(val,sys->non_gating_variables[2].x,sys->non_gating_variables[0].x,i,sys->V);
    }
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        double val = sys->non_gating_variables[i].x;
        sys->non_gating_variables[i].x = val+dt*0.5*k1_ngv[i];
    }

    // For V:
    Vk1 = sys->dV_dt(dt,t);
    sys->V = sys->V+dt*0.5*Vk1;


    // // // // // //
    // finding k2  //
    // // // // // //

    // For the gating variables:
    for(int i=0;i<sys->nr_of_gating_vars;i++){
        k2_gv[i] = sys->gating_variables[i].rhs(sys->gating_variables[i].x+dt*0.5*k1_gv[i],sys->V);
        sys->gating_variables[i].x = sys->gating_variables[i].x+dt*0.5*k2_gv[i];
    }
    // For the non-gating variables:
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        double val = sys->non_gating_variables[i].x;
        k2_ngv[i] = sys->non_gating_variables[i].rhs(val+dt*0.5*k1_ngv[i],sys->non_gating_variables[2].x,sys->non_gating_variables[0].x,i,sys->V);
    }
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        double val = sys->non_gating_variables[i].x;
        sys->non_gating_variables[i].x = val+dt*0.5*k2_ngv[i];
    }
    // For V:
    //Vk2 = Vk1; // If input is synaptic, use this instead
    Vk2 =sys->dV_dt(dt,t+dt*0.5);
    sys->V = sys->V+dt*0.5*Vk2;

    // // // // // //
    // finding k3  //
    // // // // // //

    // For the gating variables:
    for(int i=0;i<sys->nr_of_gating_vars;i++){
        k3_gv[i] = sys->gating_variables[i].rhs(sys->gating_variables[i].x+dt*0.5*k2_gv[i],sys->V);
        //sys->gating_variables[i].x = sys->gating_variables[i].x+dt*k1_gv[i];
    }
    // For the non-gating variables:
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        double val = sys->non_gating_variables[i].x;
        k3_ngv[i] = sys->non_gating_variables[i].rhs(val+dt*0.5*k2_ngv[i],sys->non_gating_variables[2].x,sys->non_gating_variables[0].x,i,sys->V);
    }
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        sys->non_gating_variables[i].x = sys->non_gating_variables[i].x+dt*0.5*k3_ngv[i];
    }
    // For V:
    //Vk3 = Vk1; // If input is synaptic, use this instead
    Vk3 = sys->dV_dt(dt,t+dt*0.5);
    sys->V = sys->V+dt*Vk3;

    // // // // // //
    // finding k4  //
    // // // // // //

    // For the gating variables:
    for(int i=0;i<sys->nr_of_gating_vars;i++){
        k4_gv[i] = sys->gating_variables[i].rhs(sys->gating_variables[i].x+dt*k3_gv[i],sys->V);
    }
    // For the non-gating variables:
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        double val = sys->non_gating_variables[i].x;
        k4_ngv[i] = sys->non_gating_variables[i].rhs(val+dt*k3_ngv[i],sys->non_gating_variables[2].x,sys->non_gating_variables[0].x,i,sys->V);
    }
    // For V:
    //Vk4 = Vk1;// If input is synaptic, use this instead
    Vk4 = sys->dV_dt(dt,t+dt);

    // // // // // // //
    // The next step: //
    // // // // // // //
    for(int i=0;i<sys->nr_of_gating_vars;i++){
        sys->gating_variables[i].x = dt/6.0*(k1_gv[i]+2*k2_gv[i]+2*k3_gv[i]+k4_gv[i]) + gv_n[i];
    }
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        sys->non_gating_variables[i].x = dt/6.0*(k1_ngv[i]+2*k2_ngv[i]+2*k3_ngv[i]+k4_ngv[i]) + ngv_n[i];
    }
    sys->V = dt/6.0*(Vk1+2*Vk2+2*Vk3+Vk4) + Vn;
}


// A function for computing a forward step in the Hodgkin-Huxley model
// using the Rush-Larsen method, which partitions the ODEs into two
// sets; the non-gating variables are treated using Forward Euler, and
// the gating variables are treated by an exponential integrator
void solver::rush_larsen(double dt, int t){

    // Computing the forward step of the voltage:
    sys->V = dt*sys->dV_dt(dt,t) + sys->V;
    double V = sys->V;

    // Computing the forward step of the non-gating variables:
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        non_gating_variable ngv = sys->non_gating_variables[i];
        sys->non_gating_variables[i].x = dt*ngv.rhs(ngv.x,sys->non_gating_variables[2].x,sys->non_gating_variables[0].x,i,V) + ngv.x; // TODO: ndv.rhs() burde ha mer generell input, kanskje array?
    }

    // Computing the forward step of the gating variables:
    for(int i=0;i<sys->nr_of_gating_vars;i++){
        gating_variable gv = sys->gating_variables[i];
        const double tau = 1.0/(gv.alpha(V)+gv.beta(V));
        const double infty = gv.alpha(V)*tau;
        sys->gating_variables[i].x = (fabs(-1.0/tau) > 1.0e-8 ? (infty + (gv.x-infty)*exp(-dt/tau)) : dt*gv.rhs(gv.x,V)+gv.x);
    }
}


// A function for computing a forward step in the Hodgkin-Huxley model
// using the explicit Euler algorithm
void solver::explicit_forward_euler(double dt, int t){
    double V = sys->V;
    // Computing the forward step of the voltage:
    sys->V = dt*sys->dV_dt(dt,t) + sys->V;

    double Q = sys->non_gating_variables[0].x;
    double P = sys->non_gating_variables[2].x;
    // Computing the forward step of the non gating variables:    
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        non_gating_variable ngv = sys->non_gating_variables[i];
        double rhs = ngv.rhs(ngv.x,P,Q,i,V);
        sys->non_gating_variables[i].x = dt*rhs+ngv.x;
    }

    // Computing the forward step of the gating variables:
    for(int i=0;i<sys->nr_of_gating_vars;i++){
        gating_variable gv = sys->gating_variables[i];
        sys->gating_variables[i].x = dt*gv.rhs(gv.x,V)+gv.x;
    }
}
// Computes the next step using forward Euler, without
// solving gating variables.
void solver::I_F_forward_Euler(double dt, int t){
    // Computing the forward step of the voltage:
    sys->V = dt*sys->dV_dt(dt,t) + sys->V;
    double V = sys->V;

    // Computing the forward step of the non gating variables:
    for(int i=0;i<sys->nr_of_non_gating_vars;i++){
        non_gating_variable ngv = sys->non_gating_variables[i];
        double rhs = ngv.rhs(ngv.x,sys->non_gating_variables[2].x,sys->non_gating_variables[0].x,i,V);
        sys->non_gating_variables[i].x = dt*rhs+ngv.x;
    }
}

// Solves the model problem using the given method
void solver::solve(double dt, int t, int freq, string method, string bn, string model, string comment){
    filewriter fw(sys,bn,model,method,t,dt,freq,comment); // new filewriter for writing to file
    cout<<"Starting:"<<endl;
    if(method=="rk4"){
        cout<<"Note that the RUnge-kutta solver scheme is currently not working with Poisson input."<<endl;
    }
    for(int i=0;i<=t;i++){
        fw.write(i); // Write this timestep to file
        // Solve for next time step using given method:
        advance(i,dt,method);
    }
}

/*
 * Advances one step using the given solver method.
 * dt is the size of the timestep used, while i is
 * the current timestep.
 */

void solver::advance(int i, double dt, string method){

    if(method.compare("fe")==0){explicit_forward_euler(dt,i);}  // Solve for next time step using explicit FE
    else if(method.compare("rl")==0){rush_larsen(dt,i);}        // Solve for next time step using RushLarsen
    else if(method.compare("IF")==0){I_F_forward_Euler(dt,i);}  // Solve for next time step using explicit FE for an integrate and fire neuron
    else{explicit_RK4(dt,i);}                // Default method ->  Solve for next time step using RungeKutta (4th order)

}




