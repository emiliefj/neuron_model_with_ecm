#ifndef NEURON_SYSTEM_H
#define NEURON_SYSTEM_H
#include"gating_variable.h"
#include"non_gating_variable.h"
#include<vector>


/*
 * A virtual class acting as an interface for a cell model.
 *
 * A cell model implementing this interface, with the model evolving
 * folloving a number of differential equations determining the time
 * evolution of the voltage, as well as a number of gating and
 * non-gating variables, can be solved by the methods in the
 * accompanying solver class.
 */
class neuron_system
{
public:
    neuron_system();


    // Attributes
    double f_in;
    double I0;

    // Gating variables:
    int nr_of_gating_vars;
    gating_variable gating_variables[3]; // Size can be changed to accommodate the specific system

    // Non-gating variables:
    int nr_of_non_gating_vars;
    non_gating_variable non_gating_variables[4]; // Size can be changed to accommodate the specific system

    // Voltage:
    double V;

    // Functions (virtual)
    virtual double I_Na() = 0;
    virtual double I_K() = 0;
    virtual double I_l() = 0;
    virtual double Imem() = 0;
    virtual double b() = 0;
    virtual double Ith() = 0;
    virtual double Isyn(int t,const double dt) = 0;
    virtual double dV_dt(const double dt, const int t) = 0;
    virtual void set_initial_values(double dt) = 0;

//protected:

    double m_aa, m_ab, m_ac, m_ad, m_af, m_ba, m_bb, m_bc, m_bd, m_bf;
    double n_aa, n_ab, n_ac, n_ad, n_af, n_ba, n_bb, n_bc, n_bd, n_bf;
    double h_aa, h_ab, h_ac, h_ad, h_af, h_ba, h_bb, h_bc, h_bd, h_bf;

};

#endif // neuron_system_H


