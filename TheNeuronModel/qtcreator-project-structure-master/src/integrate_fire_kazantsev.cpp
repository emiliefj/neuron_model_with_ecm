#include "integrate_fire_kazantsev.h"

integrate_fire_kazantsev::integrate_fire_kazantsev()
{
    this->nr_of_non_gating_vars = 0;
    Iin = 1.544; // Initial value of Iin, can be changed using the set_Iin-method
}

double integrate_fire_kazantsev::Isyn(int t,const double dt){
    return Iin;   //\mu A
}


void integrate_fire_kazantsev::set_Iin(double val){
    this->Iin = val;
}
