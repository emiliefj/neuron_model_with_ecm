#ifndef FILEWRITER_H
#define FILEWRITER_H
#include"neuron_system.h"
#include <iostream>
#include <cmath>
#include <fstream>
#include <stdlib.h>

using namespace std;

class filewriter
{
public:
    // parameters:
    double dt;         // timestep
    int T;             // The total nr of timesteps
    int writing_freq;  // How often results are written to file
    int freq_count;    // Help variable for writing_freq
    string base_name;  // The chosen base for the filename
    string model;      // "hh", "kaz"
    string method;     // "rk","rl","rk4"
    string var;        // eg. V,Q etc.
    string comment;    // Anything the user wants to specify at the beginning of the file
    bool first;
    double V,Q,R,P,Z,Ith,b;

    ofstream myfile;
    string filename;

    neuron_system* mysys;

    // Functions:
    filewriter(neuron_system *sys, string base_name, string model, string method, int T, double dt, int freq, string comment);
    void write(double t);
    void write_line(string line);
};

#endif // FILEWRITER_H
