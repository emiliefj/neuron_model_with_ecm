#include "non_gating_variable.h"

using namespace std;



/*
 * A class for containing the non gating variables of a neuron model.
 * The gating variables are the variables in the model which are
 * neither the voltage nor gating variables. In this implementation
 * they obey the (fairly general) differential equation:
 *
 * dX/dt = -(A+B*Pval)*myval + C*H(Qval),
 *
 * Where A, B and C are constants set in the constructor, Pval and
 * Qval are the current values of specific other non gating variables,
 * and myval is the current value of this non gating varible.
 *
 * H() is an activation function on the following form:
 *
 * H(Q) = x0-((x0-x1)/(1+exp(-(Q-theta)/k))).
 *
 * Here x0, x1, theta and k are constants also given upon construction.
 *
 * This implementation is made with the Kazantsev model in mind, but should
 * be general enough to accomodate several types of non gating variables,
 * although those controlled by an activation function are most easily adapted
 * to this mold.
 */


non_gating_variable::non_gating_variable()
{
}

/* Constructor where the various constants are set */
non_gating_variable::non_gating_variable(double initial_value,double A,double B,double C, double x0, double x1, double theta, double k){

    // Set constants:
    this->A = A;    // alpha_x
    this->B = B;    // gamma
    this->C = C;    // beta_x
    this->x0 = x0;  // asymptotic level with Q = infty
    this->x1 = x1;  // asymptotic level with Q = -infty
    this->theta = theta;
    this->k = k;

    this->x = initial_value;
}

/*
 * Returns the right hand side of the differential equation giving the time-
 * development og the non gating variable.
 */
double non_gating_variable::rhs(double myval,double Pval,double Qval, int myindex, double V){
    if(myindex==0){ // We are in Q -> needs V
        Qval = V;
    }
    double RHS = -(A+B*Pval)*myval + C*H(Qval);
    return RHS;
}

/* Gives the value of an activation function for input variable Q. */
double non_gating_variable::H(double Q){
   return x0-((x0-x1)/(1+exp(-(Q-theta)/k)));
}

/* A simple set-function */
void non_gating_variable::set_A(double val){
    A = val;
}

double non_gating_variable::get_value(){
    return x;
}
