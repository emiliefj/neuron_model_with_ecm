#ifndef KAZANTSEV_H
#define KAZANTSEV_H
#include"hodgkinhuxley.h"
#include"non_gating_variable.h"


/*
 * A class for the neuron model given in "A Homeostatic Model of Neuronal
 * Firing Governed by Feedback Signals from the Extracellular Matrix", Kazantsev et al.
 * The class inheriths the hodgkinhucley class, which again implements the virtual
 * class neuron_system.
 * As such the system given in this class can be solved using the methods in the
 * accompanying solver-class.
 */

class kazantsev : public hodgkinhuxley
{
public:
    // Variables:
    double spike_time;
    double EPSC; // The Excitatory Post-Synaptic Current
    double b0;   // scaling factor accounting foreffective strength of synaptic input
    double tau;  // spike duration
    //double f_in; //Charachteristic frequency of Poisson distribution
    double uniform_number;
    double gamma_Z;
    double gamma_ZR;
    //double I0;
    // Z:
    double alpha_z;
    double gamma_p;
    double beta_z;
    double z0;
    double z1;
    double theta_z;
    double k_z;
    // P:
    double alpha_p;
    double gamma;
    double beta_p;
    double p0;
    double p1;
    double theta_p;
    double k_p;
    // R:
    double alpha_r;
    double beta_r;
    double r0;
    double r1;
    double theta_r;
    double k_r;
    // Q:
    double alpha_q; // a rate constannt for average activity Q
    double beta_q;  // a scaling coefficient for average activity Q
    double q0;
    double q1;
    double theta_q;
    double k_q;

    int count;
    bool double_spike;
    double spike_time_new;
    double EPSC_new;
    vector<double> spike_train;

    double spikesum;

    double Ipulse;


    kazantsev();
    void set_initial_values(double dt);
    void set_b(double val);
    void set_Ith(double val);
    void set_fin(double val);
    void set_gammaZ(double val);
    void set_gammaZR(double val);
    void set_alphaR(double val);
    void set_alphaZ(double val);
    void set_alphaP(double val);
    void set_Q(double val);
    void set_Z(double val);
    void set_P(double val);
    void set_R(double val);
    void set_seed(double val);
    void set_gating_variable_values(double m,double n, double h);

    double b();
    double Isyn(int t, double dt);
    double Ith();
    double dV_dt(double dt, int t);
    double draw_interspike_time();
    double draw_uniform_number();
    double draw_current_amplitude();

};

#endif // KAZANTSEV_H
