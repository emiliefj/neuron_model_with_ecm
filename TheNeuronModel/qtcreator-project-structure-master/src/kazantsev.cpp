#include "kazantsev.h"
#include <iostream>
#include <cmath>

#include <stdlib.h>     /* srand, rand */
#include <ctime>
#include<vector>
//#include <random>

using namespace std;


/*
 * A class for the neuron model given in "A Homeostatic Model of Neuronal
 * Firing Governed by Feedback Signals from the Extracellular Matrix", Kazantsev et al.
 * This model models how the ECM affects the firing behaviour of the neuron it surrounds,
 * and vice versa.
 *
 * In this model the spike generation in the neuron is modeled using the standard
 * Hodgkin-Huxley formalism, with synaptic input as well as a threshold current Ith:
 *
 * C*dV/dt = -(Imem + Ith -Isyn)
 *
 * Here the synaptic input is given in the form of a Poisson spike train, The strength
 * of this synaptic input is given by b, which obeys the relation:
 *
 * b(ZR) = b_0(1+g_ZR*Z*R).
 *
 * The applied threshold current follows the realtion:
 *
 * Ith(Z) = I_0(1+g_Z*Z).
 *
 * The gating variables in the model are; Q, representing the average activity of the neuron;
 * Z, the concentration of ECM molecules; R, the concentration of ECM receptors; an P, the
 * concentration of proteases. THey obey the following differential equations:
 *
 * dQ/dt = -a_Q*Q+b_Q*H_Q(V); H_Q(V) = 1/(1+exp(-V/k_q)),
 *
 * dV/dt = -(a_z+g_p*P)*Z + b_z*H_z(Q),
 * dR/dt = -a_p*P + b_p*H_p(Q),
 * dR/dt  =-a_r*R+b_r*H_r(Q).
 *
 * Here:
 *
 * H_x(Q)  =x0 - (x0-x1)/(1+exp(-(Q-theta_x)/k_x)).
 */


kazantsev::kazantsev()
{
    /* Constructor */

    this->Ipulse = 0;
}

/*
 * Setting the values for the constants and variables
 * in the system.
 */
void kazantsev::set_initial_values(double dt){

    // Initial values for gating variables and voltage
    double m = 0.05;  // m
    double h = 0.6;   // h
    double n = 0.325; // n
    this->V = -65.0;   // V ->mV


    // The constants for the alpha and beta functions
    // of the gating variables:
    // The parameters as given in box 3.5, p 61, Sterrat
    // m:
    this->m_aa = -4;
    this->m_ab = -0.1;
    this->m_ac = -4;
    this->m_ad = -0.1;
    this->m_af = -1;

    this->m_ba = 4.0;
    this->m_bb = 0.0;
    this->m_bc = 65.0/18;
    this->m_bd = 1.0/18;
    this->m_bf = 0.0;

    // n:
    this->n_aa = -0.55;
    this->n_ab = -0.01;
    this->n_ac = -5.5;
    this->n_ad = -0.1;
    this->n_af = -1;

    this->n_ba = 0.125;
    this->n_bb = 0;
    this->n_bc = 0.8125;
    this->n_bd = 0.0125;
    this->n_bf = 0.0;

    // h:
    this->h_aa = 0.07;
    this->h_ab = 0.0;
    this->h_ac = 3.25;
    this->h_ad = 0.05;
    this->h_af = 0.0;

    this->h_ba = 1.0;
    this->h_bb = 0;
    this->h_bc = -3.5;
    this->h_bd = -0.1;
    this->h_bf = 1;

    // Initializing the gating variables:
    this->nr_of_gating_vars = 3;
    this->gating_variables[0] = gating_variable(m,m_aa,m_ab,m_ac,m_ad,m_af,m_ba,m_bb,m_bc,m_bd,m_bf);  // m
    this->gating_variables[1] = gating_variable(h,h_aa,h_ab,h_ac,h_ad,h_af,h_ba,h_bb,h_bc,h_bd,h_bf);  // h
    this->gating_variables[2] = gating_variable(n,n_aa,n_ab,n_ac,n_ad,n_af,n_ba,n_bb,n_bc,n_bd,n_bf);  // n

    // For the kazantsev-model: (parametere må tilpasses for formål)
    srand(time(NULL));
    f_in = 0.25;     // kHz
    tau = 1.0;       // msec
    spike_time = draw_interspike_time();

    // B:
    b0 = 6.0;
    gamma_ZR = 0.065;//0.02; //0.0;
    // Ith:
    I0 = 4.5; // microA/cm^2
    gamma_Z = 0.0345; // ?
    // Z:
    alpha_z = 1.0; //0.001; //msec^-1
    gamma_p = 0.1;
    beta_z = 10; //0.01;   //msec^-1
    z0 = 0;
    z1 = 1;
    theta_z = 6.5;//1;
    k_z = 0.15;
    double Z0 = 0.0;
    // P:
    alpha_p = 0.001;
    gamma = 0;
    beta_p = 0.01;   //msec^-1
    p0 = 0;
    p1 = 1;
    theta_p = 7.0;//1.5;
    k_p = 0.05;
    double P0 = 0.0;
    // R:
    alpha_r = 0.01;  //msec^-1
    beta_r = 0.01;   //msec^-1
    r0 = 2.0;
    r1 = 1.0;
    theta_r = 7.3;//1.8;
    k_r = 0.1;
    double R0 = 0.0;//0.0;
    // Q:
    alpha_q = 0.0001;// msec^-1
    beta_q = 0.01;   // msec^-1
    q0 = 0;
    q1 = 1;
    theta_q = 0;
    k_q = 0.01;
    double Q0 = 0.0;//1.50;

    Cm = 1.0;

    count = 0;
    this->spike_train.resize(1/dt);

    this->spikesum = 0;

    this->g_Na = 120;   // mS/cm^2
    this->V_Na = 50;    //115.-65 mV
    this->g_K = 36;     // mS/cm^2
    this->V_K = -77;    //-12.-65 mV
    this->g_l = 0.3;    // mS/cm^2
    this->V_l = -54.4;  //10.613-65 mV
    this->Cm = 1;


    // Initializing the non gating variables:
    this->nr_of_non_gating_vars = 4;
    this->non_gating_variables[0] = non_gating_variable(Q0, alpha_q, gamma,  beta_q, q0, q1, theta_q, k_q);  // Q
    this->non_gating_variables[1] = non_gating_variable(Z0, alpha_z, gamma_p,beta_z, z0, z1, theta_z, k_z);  // Z
    this->non_gating_variables[2] = non_gating_variable(P0, alpha_p, gamma,  beta_p, p0, p1, theta_p, k_p);  // P
    this->non_gating_variables[3] = non_gating_variable(R0, alpha_r, gamma,  beta_r, r0, r1, theta_r, k_r);  // R

}

/*
 * Returns the synaptic input at the current time step, t.
 *
 * The variable spike_time keeps track of the start time of the next
 * incoming spike. If the current time is later that spike_time, a
 * new spike_time is drawn using the draw_interspike_time()-function.
 * The amplitude of the spike is determined by the draw_current_amplitude-
 * function.
 * The vector spike_train holds the values of the input for every
 * timestep the following 1ms. When a new spike amplitude is drawn its
 * amplitude is added to every element of this array, as a spike is
 * 1ms long.
 *
 * The synaptic input is returned as the value of the spike train at the
 * current time step.
 */
double kazantsev::Isyn(int t,double dt){
    int length = 1/dt; // length of the spike train vector, 1ms/dt
    spike_train.resize(length);
    int index = t%length; // current index in the spike_train-array

    if((tau+t*dt)>=spike_time){ // draw new spike_time
        count++;
        spike_time = spike_time + draw_interspike_time();
        EPSC = draw_current_amplitude();
        for (int i=0; i < length; i++) {
            spike_train[i] += EPSC; // add the input to every element in the spiketrain (wich is one spike long):
        }
    }
    double synaptic_input = spike_train[index];
    spike_train[index] = 0;
    //spikesum += synaptic_input;
    //cout<<synaptic_input<<"  t: "<<t<<"  Q: "<<non_gating_variables[0].x<<endl;
    return synaptic_input+Ipulse;
}

/*
 * Returns the time until the next spike. The times are drawn
 * following a Poisson distribution.
 */
double kazantsev::draw_interspike_time(){
    uniform_number = draw_uniform_number();
    return -log(1.0-uniform_number)/f_in; // time until next spike
}

/*
 * Returns the amplitude of the a spike. The amplitude is drawn
 * following a Poisson distribution.
 */
double kazantsev::draw_current_amplitude(){
    uniform_number = draw_uniform_number();
    return b()*sqrt(-log(1-uniform_number)); // Amplitude of the EPSC
}

/*
 *  Returns a uniform random number between 0 and 1.
 */
double kazantsev::draw_uniform_number(){
    return (rand() % 1000)/1000.0;
}

/*
 * Returns the current value of the scaling parameter b.
 */
double kazantsev::b(){
    double Z = non_gating_variables[1].x;
    double R = non_gating_variables[3].x;

    return b0*(1+gamma_ZR*Z*R);
}

/*
 * Returns the current value of the applied threshold current, Ith.
 */
double kazantsev::Ith(){
    double Z = non_gating_variables[1].x;

    return I0*(1+gamma_Z*Z);
}

/* Returns the value for the RHS of the voltage equation. */
double kazantsev::dV_dt(double dt, int t){
    double rhs = -(Imem()+Ith()-Isyn(t,dt))/(Cm);
    //cout<<rhs<<endl;
    return rhs;
}


void kazantsev::set_b(double val){
    b0 = val;
}

void kazantsev::set_Ith(double val){
    I0 = val;
}

void kazantsev::set_fin(double val){
    f_in = val;
}

void kazantsev::set_gammaZ(double val){
    gamma_Z = val;
}

void kazantsev::set_gammaZR(double val){
    gamma_ZR = val;
}

void kazantsev::set_alphaP(double val){
    non_gating_variables[2].set_A(val);
}

void kazantsev::set_alphaZ(double val){
    non_gating_variables[1].set_A(val);
}

void kazantsev::set_alphaR(double val){
    non_gating_variables[3].set_A(val);
}

void kazantsev::set_Q(double val){
    non_gating_variables[0].x = val;
}

void kazantsev::set_Z(double val){
    non_gating_variables[1].x = val;
}

void kazantsev::set_P(double val){
    non_gating_variables[2].x = val;
}

void kazantsev::set_R(double val){
    non_gating_variables[3].x = val;
}

void kazantsev::set_seed(double val){
    srand(val);
}

void kazantsev::set_gating_variable_values(double m,double n, double h){
    gating_variables[0].x = m;
    gating_variables[1].x = h;
    gating_variables[2].x = n;
}






