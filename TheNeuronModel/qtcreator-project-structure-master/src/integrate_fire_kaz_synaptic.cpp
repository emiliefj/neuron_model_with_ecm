#include "integrate_fire_kaz_synaptic.h"

integrate_fire_kaz_synaptic::integrate_fire_kaz_synaptic()
{
    /* Constructor*/
}

/*
 * Setting the values for the constants and variables
 * in the system.
 */
void integrate_fire_kaz_synaptic::set_initial_values(double dt){

    // Initial values for gating variables and voltage
    double m = 0.05;  // m
    double h = 0.6;   // h
    double n = 0.325; // n
    V = -65.0;   // V ->mV


    // The constants for the alpha and beta functions
    // of the gating variables:
    // The parameters as given in box 3.5, p 61, Sterrat
    // m:
    this->m_aa = -4;
    this->m_ab = -0.1;
    this->m_ac = -4;
    this->m_ad = -0.1;
    this->m_af = -1;

    this->m_ba = 4.0;
    this->m_bb = 0.0;
    this->m_bc = 65.0/18;
    this->m_bd = 1.0/18;
    this->m_bf = 0.0;

    // n:
    this->n_aa = -0.55;
    this->n_ab = -0.01;
    this->n_ac = -5.5;
    this->n_ad = -0.1;
    this->n_af = -1;

    this->n_ba = 0.125;
    this->n_bb = 0;
    this->n_bc = 0.8125;
    this->n_bd = 0.0125;
    this->n_bf = 0.0;

    // h:
    this->h_aa = 0.07;
    this->h_ab = 0.0;
    this->h_ac = 3.25;
    this->h_ad = 0.05;
    this->h_af = 0.0;

    this->h_ba = 1.0;
    this->h_bb = 0;
    this->h_bc = -3.5;
    this->h_bd = -0.1;
    this->h_bf = 1;

    // Initializing the gating variables:
    this->nr_of_gating_vars = 3;
    this->gating_variables[0] = gating_variable(m,m_aa,m_ab,m_ac,m_ad,m_af,m_ba,m_bb,m_bc,m_bd,m_bf);  // m
    this->gating_variables[1] = gating_variable(h,h_aa,h_ab,h_ac,h_ad,h_af,h_ba,h_bb,h_bc,h_bd,h_bf);  // h
    this->gating_variables[2] = gating_variable(n,n_aa,n_ab,n_ac,n_ad,n_af,n_ba,n_bb,n_bc,n_bd,n_bf);  // n

    this->nr_of_non_gating_vars = 0;

    // For the kazantsev-model: // Husk: parametere må tilpasses for formål
    srand(time(NULL));
    f_in = 0.25;     // kHz
    tau = 1.0;      // msec
    spike_time = draw_interspike_time();

    // B:
    b0 = 6.0;
    gamma_ZR = 0.02; //0.0;
    // Ith:
    I0 = 4.5; // microA/cm^2
    gamma_Z = 0.0345; // ?
    // Z:
    alpha_z = 1.0; //0.001; //msec^-1
    gamma_p = 0.1;
    beta_z = 10; //0.01;   //msec^-1
    z0 = 0;
    z1 = 1;
    theta_z = 6.5;//1;
    k_z = 0.15;
    double Z0 = 0.0;
    // P:
    alpha_p = 0.001;
    gamma = 0;
    beta_p = 0.01;   //msec^-1
    p0 = 0;
    p1 = 1;
    theta_p = 7.0;//1.5;
    k_p = 0.05;
    double P0 = 0.0;
    // R:
    alpha_r = 0.01;  //msec^-1
    beta_r = 0.01;   //msec^-1
    r0 = 2.0;
    r1 = 1.0;
    theta_r = 7.3;//1.8;
    k_r = 0.1;
    double R0 = 0.0;
    // Q:
    alpha_q = 0.0001;// msec^-1
    beta_q = 0.01;   // msec^-1
    q0 = 0;
    q1 = 1;
    theta_q = 0;
    k_q = 0.01;
    double Q0 = 0.0;//1.50;

    Cm = 1.0;

    count = 0;
    this->spike_train.resize(1/dt);

    this->spikesum = 0;

    this->g_Na = 120;   // mS/cm^2
    this->V_Na = 50;    //115.-65 mV
    this->g_K = 36;     // mS/cm^2
    this->V_K = -77;    //-12.-65 mV
    this->g_l = 0.3;    // mS/cm^2
    this->V_l = -54.4;  //10.613-65 mV
    this->Cm = 1;


    // Initializing the non gating variables:
    this->nr_of_non_gating_vars = 4;
    this->non_gating_variables[0] = non_gating_variable(Q0, alpha_q, gamma,  beta_q, q0, q1, theta_q, k_q);  // Q
    this->non_gating_variables[1] = non_gating_variable(Z0, alpha_z, gamma_p,beta_z, z0, z1, theta_z, k_z);  // Z
    this->non_gating_variables[2] = non_gating_variable(P0, alpha_p, gamma,  beta_p, p0, p1, theta_p, k_p);  // P
    this->non_gating_variables[3] = non_gating_variable(R0, alpha_r, gamma,  beta_r, r0, r1, theta_r, k_r);  // R


    // Unique for integrate_fire_kazantsev:

    this->Vm = -64; // mV
    this->Rm = 22;  // k\Ohm
    this->thresh = -49; // mV, the threshold
}



double integrate_fire_kaz_synaptic::dV_dt(double dt, int t){
    check_threshold();
    double rhs = -(I_RC()-Isyn(t,dt))/(Cm);
    return rhs;
}


double integrate_fire_kaz_synaptic::I_RC(){
    return (V-Vm)/Rm;
}


/*
 * Checks if the threshold is passed, andif so, resets the membrane potential.
 * Increases also Q by dQ if a spike has indeed ocurred.
 *
 */
double integrate_fire_kaz_synaptic::check_threshold(){
    if(V>thresh){ // Spike!
        non_gating_variables[0].x += beta_q*tau; // Increasing Q
        V = Vm;

    }
}
