#ifndef KAZANTSEV_PULSE_H
#define KAZANTSEV_PULSE_H
#include"kazantsev.h"


/*
 * A class for the neuron model given in "A Homeostatic Model of Neuronal
 * Firing Governed by Feedback Signals from the Extracellular Matrix", Kazantsev et al.,
 * except the Poisson train synaptic input is replaced by a simple pulse,
 * The class inheriths the kazantsev class.
 *
 * The system given in this class can be solved using the methods in the accompanying
 * solver-class.
 */

class kazantsev_pulse : public kazantsev
{
public:
    kazantsev_pulse();
    double Isyn(int t, double dt);
    void set_Isyn_amplitude(double val); // The amplitude of the synaptic input-pulse
    void set_Isyn_time(double tmin,double tmax); // The pulse lasts from tmin to tmax, tmin and tmax given in ms

    double Isyn_amp;
    double Isyn_start;
    double Isyn_end;
};

#endif // KAZANTSEV_PULSE_H
