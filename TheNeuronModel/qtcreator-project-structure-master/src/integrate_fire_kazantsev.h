#ifndef INTEGRATE_FIRE_KAZANTSEV_H
#define INTEGRATE_FIRE_KAZANTSEV_H
#include"integrate_fire_kaz_synaptic.h"

class integrate_fire_kazantsev : public integrate_fire_kaz_synaptic
{
    /*
     * The Kazantsev model, but simplified so that instead
     * of using Hodgkin-Huxley to model the activity of the
     * neuron, a simple integrate and fire model is used.
     */

public:
    integrate_fire_kazantsev();
    double Isyn(int t,const double dt);
    void set_Iin(double val);

    double Iin=0;

};

#endif // INTEGRATE_FIRE_KAZANTSEV_H
