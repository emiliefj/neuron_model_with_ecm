include(../defaults.pri)

CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt
CONFIG   += c++11

TEMPLATE = app

SOURCES += main.cpp \
    reproduce_kaz_figs.cpp \
    special_solver_methods.cpp

LIBS += -L../src -lmyapp

HEADERS += \
    reproduce_kaz_figs.h \
    special_solver_methods.h
