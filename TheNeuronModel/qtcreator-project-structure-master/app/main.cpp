#include <iostream>
#include <cmath>
#include <fstream>
#include <stdlib.h>
#include <time.h>


#include"hodgkinhuxley.h"
#include"kazantsev.h"
#include"kazantsev_pulse.h"
#include"integrate_fire_kaz_synaptic.h"
#include"integrate_fire_kazantsev.h"
#include"solver.h"
#include"neuron_system.h"

#include"reproduce_kaz_figs.h"
#include "special_solver_methods.h"

using namespace std;

/*
 * Solves the Hodgkin-Huxley model using the given method, with timestep dt
 * for a total time t. The freq parameter indicates how often the result
 * should be written to file. By adjusting this parameter you can assure
 * the output file does not become too large.
 */
void solve_HH(double dt, int t, int freq, string method){ // t=total time
    hodgkinhuxley sys;
    sys.set_initial_values(dt);
    solver mysolver(&sys);
    string model = "HH";
    string bn = "solving";
    string comment = "HodgkinHuxley solved with " + method + " and timestep dt="+to_string(dt);
    double timesteps = t/dt; // Finding the number of timesteps to be calculated

    mysolver.solve(dt,timesteps,freq,method,bn,model,comment);
}


/*
 * Solves the standard Kazantsev model using the given method, with
 * timestep dt for a total time t. The freq parameter indicates how often
 * the result should be written to file. By adjusting this parameter you
 * can assure the output file does not become too large.
 */
void solve_kazantsev(double dt, int t, int freq, string method){ // t=total time
    kazantsev sys;
    double chosen_fin = 2.0;
    double chosen_b0 = 6;
    sys.set_initial_values(dt);
    sys.set_fin(chosen_fin);
    sys.set_b(chosen_b0);
    solver mysolver(&sys);

    string model = "kaz";
    string bn = "running_f_in_" + to_string(chosen_fin) + "_b_"+ to_string(chosen_b0);
    string comment = "The Kazantsev model solved with " + method + " and timestep dt="+to_string(dt);
    double timesteps = t/dt; // Finding the number of timesteps to be calculated

    mysolver.solve(dt,timesteps,freq,method,bn,model,comment);
}


/*
 * Solves the Kazantsev model using an integrate-and-fire neuron in place
 * of the HodgkinHuxley model. The model is solved using the given method,
 * with timestep dt for a total time t. The synaptic input is in the form
 * of a pulse.
 * The freq parameter indicates how often the result should be written to
 * file. By adjusting this parameter you can assure the output file does
 * not become too large.
 */
void solve_integrate_fire(double dt, int t, int freq, string method){ // t=total time
    integrate_fire_kazantsev sys;
    sys.set_initial_values(dt);
    double Iin = 1.544;
    sys.set_Iin(Iin);//1.3);
    solver mysolver(&sys);

    string model = "kaz";
    string bn = "running_integrate_fire_constant_I_"+to_string(Iin);
    string comment = "The Kazantsev model with the integrate-and-fire neuron model solved with " + method + " and timestep dt="+to_string(dt);
    double timesteps = t/dt; // Finding the number of timesteps to be calculated

    mysolver.solve(dt,timesteps,freq,method,bn,model,comment);
}

/*
 * Solves the Kazantsev model without any regular synaptic input. Istead
 * a strong pulse is added after a time start_t lasting until end_t. The
 * pulse has tha amplitude amp.
 * The model is solved using the given method, with timestep dt for a
 * total time t. The freq parameter indicates how often the result should
 * be written to file. By adjusting this parameter you can assure the
 * output file does not become too large.
 */
void solve_kazantsev_pulse(double dt, int t, int start_t, int end_t, int amp, int freq, string method){
    kazantsev_pulse sys;
    sys.set_initial_values(dt);
    sys.set_Isyn_amplitude(amp);
    sys.set_Isyn_time(start_t,end_t);
    solver mysolver(&sys);

    string model = "kaz";
    string bn = "running_kaz_pulse";
    string comment = "Kazantsev model with only synaptic input in the form of a pulse of size " + to_string(amp) + ", starting at " + to_string(start_t);
    double timesteps = t/dt; // Finding the number of timesteps to be calculated

    mysolver.solve(dt,timesteps,freq,method,bn,model,comment);
}

/*
 * Times the runtime for integrate_fire_kazantsev vs kazantsev when running
 * for the given amount of time t.
 */
void time_HH_vs_IF(double dt, int t, int freq, string method){
    clock_t start_t = clock();
    cout<<"Timing integrate_fire_kazantsev running for "<<t/1000<<" sec with dt="<<dt<<" and using "<<method<<" as the numerical scheme"<<endl;
    solve_integrate_fire(dt,t,freq,"IF");
    clock_t end_t = clock();
    printf ("Total runtime was %f seconds.\n \n",((float)(end_t-start_t))/CLOCKS_PER_SEC);

    start_t = clock();
    cout<<"Timing kazantsev running for "<<t/1000<<" sec with dt="<<dt<<" and using "<<method<<" as the numerical scheme"<<endl;
    solve_integrate_fire(dt,t,freq,method);
    end_t = clock();
    printf ("Total runtime was %f seconds.\n",((float)(end_t-start_t))/CLOCKS_PER_SEC);
}



int main()
{
//    reproduce_fig5("fe",0.1);
//    investigate_interspike_times(0.2);
//    reproduce_fig1A(10);
//    reproduce_fig1A(10);
//    reproduce_fig2B(0.05);
//    reproduce_fig5("fe",0.05);
    //solve_kazantsev(0.0004,10000,1000,"rk4");
//    solve_kazantsev_high_activity(0.0001,60000,10000,"fe");
//    solve_integrate_fire(0.0002,90000,10000,"fe");
//    solve_integrate_fire_synaptic(0.0005,90000,10000,"rl");
//    solve_kazantsev_high_activity(0.0005,120000,10000,"rl");
//    solve_kazantsev_pulse(0.0001,30000,10000,"rl");
//    reproduce_fig2B_IF(0.05);
//    time_HH_vs_IF(0.001,600000,10000,"fe");
//    run_adding_prot_enzyme(0.005,60000,40000,1000,0.005,"rl");
//    run_adding_receptor_enzyme(0.005,60000,40000,1000,0.05,"rl");
//    run_adding_ECM_enzyme(0.001,180000,60000,1000,6,"rl");
//    solve_HH(0.0001,50,10,"fe");
//    solve_HH(0.0001,50,10,"rk4");
//    solve_HH(0.0001,50,10,"rl");
    solve_kazantsev(0.0001,10000,10000,"rk4");
//    solve_kazantsev_normal_plus_pulse(0.0002, 50000,5000, 10000,"fe");



}
