#ifndef SPECIAL_SOLVER_METHODS_H
#define SPECIAL_SOLVER_METHODS_H

#include <iostream>
#include <cmath>
#include <fstream>
#include <stdlib.h>
#include <time.h>


#include"hodgkinhuxley.h"
#include"kazantsev.h"
#include"kazantsev_pulse.h"
#include"integrate_fire_kaz_synaptic.h"
#include"integrate_fire_kazantsev.h"
#include"solver.h"
#include"neuron_system.h"


void solve_kazantsev_high_activity(double dt, int t, int freq, string method);  // Solves the Kazantsev model in the high activity regime
void solve_integrate_fire_synaptic(double dt, int t, int freq, string method);  // Solves the Kazantsev model with an IF neuron
void solve_kazantsev_normal_plus_pulse(double dt, int t, int tstart,int freq, string method);       // Solves the Kazantsev model, with an extra pulse input current
void run_adding_prot_enzyme(double dt, int t, int enz_t,int freq,double enz_val, string method);    // Simulates the adding of a enzyme degrading the proteases of the ECM
void run_adding_ECM_enzyme(double dt, int t, int enz_t,int freq,double enz_val, string method);     // Simulates the adding of a enzyme degrading the ECM molecules
void run_adding_receptor_enzyme(double dt, int t, int enz_t,int freq,double enz_val, string method);// Simulates the adding of a enzyme degrading the receptors of the ECM

#endif // SPECIAL_SOLVER_METHODS_H
