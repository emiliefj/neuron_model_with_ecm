#include"reproduce_kaz_figs.h"
#include <vector>


using namespace std;

/*
 * A function for reproducing figure 1A in Kazantsev et al. 2012.
*/
void reproduce_fig1A(double b){

    int N = 100000000;
    kazantsev mysys;
    mysys.set_initial_values(0.05);
    mysys.set_b(b);
    string basename = "EPSCamplitudes_N_" + to_string(N) + "_b_" + to_string(b);
    string comment = "Amplitudes of the EPSC when b="+ to_string(b);
    filewriter fw(&mysys,basename,"","",N,b,0,comment); // new filewriter for writing to file

    double amplitude = 0;
    for(int i=0;i<N;i++){
        amplitude = mysys.draw_current_amplitude();
        fw.write_line((to_string(amplitude)));
    }
}

/*
 * A function for excamining the distribution of the interspike times.
*/
void investigate_interspike_times(double f_in){

    int N = 100000000;
    kazantsev mysys;
    mysys.set_initial_values(0.05);
    mysys.set_fin(f_in);
    string basename = "interspiketimes_fin_" + to_string(f_in);
    string comment = "Interspike times for synaptic input when f_in="+ to_string(f_in);
    filewriter fw(&mysys,basename,"","",N,f_in,0,comment); // new filewriter for writing to file

    double spiketime = 0;
    for(int i=0;i<N;i++){
        spiketime = mysys.draw_interspike_time();
        fw.write_line((to_string(spiketime)));
    }
}

/*
 * A function for reproducing figure 2B in Kazantsev et al. 2012
 * Runs two min simulations for each value of f_in, and writes the
 * average Q-value for each simulation to file.
 * In the simulation b=6, and Ith=4.5.
*/
void reproduce_fig2B(double step){

    // Declearing parameters:
    double tot_time = 120000.0; // msec, total time per f_in simulation
    double dt = 0.0005;          // msec, size of time step
    int freq = 0;
    const int T = tot_time/dt;        // nr of timesteps
    int T_start_calc = 60000.0/dt;    // Wait this long (one min) before Q_avg is calculated (to ensure steady state)

    double f_in = 0.0;      // kHz, starting freq
    double f_in_max = 2.0;  // kHz, max freq
    double f_step = step;    // freq step size
    double nr_of_f_steps = (f_in_max-f_in)/f_step;  // Total number of frequency steps

    double b = 6;
    double Ith = 4.5;//4.5;

    double Q_avg;
    int j;
    string method = "fe";   // Solver method

    // Info for writing to file:
    string base_name  = "recfig2B_b_" + to_string(b) + "_Ith_" + to_string(Ith);
    string model = "kaz";
    string comment = "Recreation of fig 2B in Kazantsev et al. 2012, b=" + to_string(b) + ", Ith = " + to_string(Ith);

    // Make the system:
    kazantsev mysys;
    // Make the solver:
    solver mysolver(&mysys);
    // Make the filewriter
    filewriter fw(&mysys,base_name,model,method,T,dt,freq,comment); // new filewriter for writing to file

    fw.write_line((to_string(nr_of_f_steps)));
    fw.write_line("  f_in:      Q_avg:");//    spike_freq:   spike duration:");

    // // // // // // // // // // //
    //  Running the simulations:  //
    // // // // // // // // // // //
    while(f_in<(f_in_max+f_step/2)){
        mysys.set_initial_values(dt);
        mysys.set_fin(f_in); // setting the input frequency
        mysys.set_b(b);
        mysys.set_Ith(Ith);
        mysys.set_gammaZ(0);
        mysys.set_gammaZR(0);
        Q_avg = 0;
        j = 0;
        for(int i=0;i<=T;i++){
            if(i>=T_start_calc){ // start calculating average Q
                Q_avg += mysys.non_gating_variables[0].x; // add current Q-value
                j++;
            }
            // Solve for next time step using given method:
            mysolver.advance(i,dt,method);
        }
        Q_avg = Q_avg/j;
        fw.write_line(to_string(f_in) + "    " + to_string(Q_avg));
        f_in = f_in + f_step;
    }
}

/*
 * A function for reproducing figure 2B in Kazantsev et al. 2012,
 * but with an integrate-and-fire neuron.
 * Runs two min simulations for each value of f_in, and writes the
 * average Q-value for each simulation to file.
 * In the simulation b=6, and Ith=4.5.
*/
void reproduce_fig2B_IF(double step){

    // Declearing parameters:
    double tot_time = 120000.0; // msec, total time per f_in simulation
    double dt = 0.0005;          // msec, size of time step
    int freq = 0;
    const int T = tot_time/dt;        // nr of timesteps
    int T_start_calc = 60000.0/dt;    // Wait this long (one min) before Q_avg is calculated (to ensure steady state)
    double f_in = 0.0;      // kHz, starting freq
    double f_in_max = 2.0;  // kHz, max freq
    double f_step = step;    // freq step size
    double nr_of_f_steps = (f_in_max-f_in)/f_step;  // Total number of frequency steps
    double b = 6;
    double Ith = 8.5;//4.5;
    double Q_avg;
    int j;
    string method = "fe";   // Solver method

    // Info for writing to file:
    string base_name  = "recfig2B_integrate_fire_b_" + to_string(b) + "_Ith_" + to_string(Ith);
    string model = "kaz";
    string comment = "Recreation of fig 2B in Kazantsev et al. 2012, b=" + to_string(b) + ", Ith = " + to_string(Ith);

    // Make the system:
    integrate_fire_kazantsev mysys;
    // Make the solver:
    solver mysolver(&mysys);
    // Make the filewriter
    filewriter fw(&mysys,base_name,model,method,T,dt,freq,comment); // new filewriter for writing to file

    fw.write_line((to_string(nr_of_f_steps)));
    fw.write_line("  f_in:      Q_avg:");//    spike_freq:   spike duration:");

    // // // // // // // // // // //
    //  Running the simulations:  //
    // // // // // // // // // // //
    while(f_in<(f_in_max+f_step/2)){
        mysys.set_initial_values(dt);
        mysys.set_fin(f_in); // setting the input frequency
        mysys.set_b(b);
        mysys.set_Ith(Ith);
        mysys.set_gammaZ(0);
        mysys.set_gammaZR(0);
        Q_avg = 0;
        j = 0;
        for(int i=0;i<=T;i++){
            if(i>=T_start_calc){ // start calculating average Q

                Q_avg += mysys.non_gating_variables[0].x; // add current Q-value
                j++;
            }
            // Solve for next time step using given method:
            mysolver.advance(i,dt,method);
        }
        Q_avg = Q_avg/j;//(T-T_start_calc);
        fw.write_line(to_string(f_in) + "    " + to_string(Q_avg));
        f_in = f_in + f_step;
    }
}


/*
 * A function for reproducing figure 5 in Kazantsev et al. 2012
 * Runs two min simulations for each value of f_in, and writes the
 * average Q-value for each simulation to file.
 * In the simulation b=10, and Ith is varied.
*/
void reproduce_fig5(string method, double step){

    // Declearing parameters:
    double tot_time = 120000.0; // msec
    double dt = 0.005;          // msec
    int T = tot_time/dt;        // nr of timesteps
    int freq = 0;
    int T_start_calc = 60000.0/dt;    // Wait this long (one min) before Q_avg is calculated (to ensure steady state)

    double I_th = 4.0;      // microAmpere/cm^2, starting value for I_th
    double I_th_max = 16.0; // microAmpere/cm^2, max value
    double I_step = step;
    double b = 10;
    vector<double> f_input = {0.1,0.2,0.3};

    double Q_avg;
    int j;

    // Info for writing to file:
    string base_name  = "recfig5_b_"+to_string(b)+ "_kHz_Ith_" + to_string(I_th) + "_" + to_string(I_th_max);
    string model = "kaz";
    string comment = "Recreation of fig 5, b="+to_string(b);

    // Making the system:
    kazantsev mysys;
    //Making the solver:
    solver mysolver(&mysys);
    // Making the filewriter;
    filewriter fw(&mysys,base_name,model,method,T,I_step,freq,comment); // new filewriter for writing to file

    //fw.write_line((to_string(nr_of_I_steps)));
    fw.write_line("  I_th:      Q_avg:");

    // // // // // // // // // // //
    //  Running the simulations:  //
    // // // // // // // // // // //
    for(double f : f_input){
        cout<<f<<endl;
        while(I_th<(I_th_max+I_step/2)){ // TODO: make this a for-loop
            mysys.set_initial_values(dt);
            mysys.set_Ith(I_th); // setting the threshold value
            mysys.set_fin(f);
            mysys.set_b(b);
            Q_avg = 0;
            j = 0;
            for(int i=0;i<=T;i++){
                if(i>=T_start_calc){ // start calculating average Q
                    Q_avg += mysys.non_gating_variables[0].x; // add current Q-value
                    j++;
                }
                // Solve for next time step using given method:
                mysolver.advance(i,dt,method);
            }
            if(Q_avg==0){}
            else{Q_avg = Q_avg/j;}
            fw.write_line(to_string(I_th) + "    " + to_string(Q_avg));

         I_th = I_th + I_step;
        }
    I_th = 4.0;
    }

}




