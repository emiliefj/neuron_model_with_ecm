#include "special_solver_methods.h"


/*
 * Solves the standard Kazantsev model using the given method, with
 * timestep dt for a total time t. The model is solven in the high
 * activity regime.
 * The freq parameter indicates how often
 * the result should be written to file. By adjusting this parameter you
 * can assure the output file does not become too large.
 */
void solve_kazantsev_high_activity(double dt, int t, int freq, string method){
    kazantsev sys;
    double f_in = 3.0;
    sys.set_initial_values(dt);
    sys.set_fin(f_in);
    solver mysolver(&sys);

    string model = "kaz";
    string bn = "running_high_activity_1min"+ to_string(f_in);
    string comment = "The Kazantsev model solved with " + method + " and timestep dt="+to_string(dt);
    double timesteps = t/dt; // Finding the number of timesteps to be calculated

    mysolver.solve(dt,timesteps,freq,method,bn,model,comment);
}


/*
 * Solves the Kazantsev model using an integrate-and-fire neuron in place
 * of the HodgkinHuxley model. The model is solved using the given method,
 * with timestep dt for a total time t. The synaptic input is in the form
 * of a Poisson spike train as for the standard Kazantsev model.
 * The freq parameter indicates how often the result should be written to
 * file. By adjusting this parameter you can assure the output file does
 * not become too large.
 */
void solve_integrate_fire_synaptic(double dt, int t, int freq, string method){
    integrate_fire_kaz_synaptic sys;
    double fin = 0.28;
    sys.set_initial_values(dt);
    sys.set_fin(fin);
    solver mysolver(&sys);

    string model = "kaz";
    string bn = "running_integrate_fire_synaptic_" + to_string(fin);;
    string comment = "Running using the integrate and fire neuron model, with synaptic input in the form of a Poisson train";
    double timesteps = t/dt; // Finding the number of timesteps to be calculated

    mysolver.solve(dt,timesteps,freq,method,bn,model,comment);
}


/*
 * Solves the Kazantsev model without regular low activity synaptic input. In
 * addition a strong pulse is added after a time tstart lasting for 2ms. The
 * pulse has the amplitude amp.
 * The model is solved using the given method, with timestep dt for a
 * total time t. The freq parameter indicates how often the result should
 * be written to file. By adjusting this parameter you can assure the
 * output file does not become too large.
 */
void solve_kazantsev_normal_plus_pulse(double dt, int t, int tstart,int freq, string method){
    kazantsev sys;
    double chosen_fin = 0.5;
    double chosen_b0 = 6;
    sys.set_initial_values(dt);
    sys.set_fin(chosen_fin);
    sys.set_b(chosen_b0);

    double pulse_amplitude=2000;
    double tslutt = tstart+2000;

    solver mysolver(&sys);

    string model = "kaz";
    string bn = "running_normal_plus_pulse_f_in_" + to_string(chosen_fin) + "_b_"+ to_string(chosen_b0);
    string comment = "Dette er en kommentar";
    double timesteps = t/dt; // Finding the number of timesteps to be calculated

    filewriter fw(&sys,bn,model,method,timesteps,dt,freq,comment); // new filewriter for writing to file
    cout<<"Starting:"<<endl;
    for(int i=0;i<=timesteps;i++){
        fw.write(i); // Write this timestep to file
        // Solve for next time step using given method:
        if(i*dt <= tslutt && i*dt >= tstart){
            sys.Ipulse = pulse_amplitude;
        }
        else{
            sys.Ipulse = 0;
        }
        mysolver.advance(i,dt,method);
    }
}

/*
 * Runs the Kazantsev model for a time t using dt as the time step and
 * method as the solver scheme. After a time enz_t, the adding of an enzyme
 * that degrades the proteases of the ECM is simulated through an increase
 * in the degration rate \alpha_p to the new value enz_val
 */
void run_adding_prot_enzyme(double dt, int t, int enz_t,int freq,double enz_val, string method){
    kazantsev sys;
    double chosen_fin = 3;
    double chosen_b0 = 6;
    sys.set_initial_values(dt);
    sys.set_fin(chosen_fin);
    sys.set_b(chosen_b0);
    solver mysolver(&sys);

    string model = "kaz";
    string bn = "running_adding_prot_enzyme_" + to_string(enz_t) + "_fin_"+ to_string(chosen_fin);
    string comment = "Dette er en kommentar";
    double timesteps = t/dt; // Finding the number of timesteps to be calculated

    double new_alphaP = enz_val;
    filewriter fw(&sys,bn,model,method,timesteps,dt,freq,comment); // new filewriter for writing to file
    cout<<"Starting:"<<endl;
    for(int i=0;i<=timesteps;i++){
        fw.write(i); // Write this timestep to file
        if(i*dt==enz_t){
            sys.set_alphaP(new_alphaP); // Simulate adding the enzyme
        }
        // Solve for next time step using given method:
        mysolver.advance(i,dt,method);
    }
}

/*
 * Runs the Kazantsev model for a time t using dt as the time step and
 * method as the solver scheme. After a time enz_t, the adding of an enzyme
 * that degrades the ECM molecules is simulated through an increase
 * in the degration rate \alpha_z to the new value enz_val
 */
void run_adding_ECM_enzyme(double dt, int t, int enz_t,int freq,double enz_val, string method){
    kazantsev sys;
    double chosen_fin = 3;
    double chosen_b0 = 6;
    sys.set_initial_values(dt);
    sys.set_fin(chosen_fin);
    sys.set_b(chosen_b0);
    solver mysolver(&sys);

    string model = "kaz";
    string bn = "running_adding_ECM_enzyme_" + to_string(enz_t) + "_fin_"+ to_string(chosen_fin);
    string comment = "Dette er en kommentar";
    double timesteps = t/dt; // Finding the number of timesteps to be calculated

    double new_alphaZ = enz_val;
    filewriter fw(&sys,bn,model,method,timesteps,dt,freq,comment); // new filewriter for writing to file
    cout<<"Starting:"<<endl;
    for(int i=0;i<=timesteps;i++){
        fw.write(i); // Write this timestep to file
        if(i*dt==enz_t){
            sys.set_alphaZ(new_alphaZ); // Simulate adding the enzyme
        }
        // Solve for next time step using given method:
        mysolver.advance(i,dt,method);
    }
}

/*
 * Runs the Kazantsev model for a time t using dt as the time step and
 * method as the solver scheme. After a time enz_t, the adding of an enzyme
 * that degrades the ECM receptors is simulated through an increase
 * in the degration rate \alpha_r to the new value enz_val
 */
void run_adding_receptor_enzyme(double dt, int t, int enz_t,int freq,double enz_val, string method){
    kazantsev sys;
    double chosen_fin = 3;
    double chosen_b0 = 6;
    sys.set_initial_values(dt);
    sys.set_fin(chosen_fin);
    sys.set_b(chosen_b0);
    solver mysolver(&sys);

    string model = "kaz";
    string bn = "running_adding_receptor_enzyme_" + to_string(enz_t) + "_fin_"+ to_string(chosen_fin);
    string comment = "Dette er en kommentar";
    double timesteps = t/dt; // Finding the number of timesteps to be calculated

    double new_alphaR = enz_val;
    filewriter fw(&sys,bn,model,method,timesteps,dt,freq,comment); // new filewriter for writing to file
    cout<<"Starting:"<<endl;
    for(int i=0;i<=timesteps;i++){
        fw.write(i); // Write this timestep to file
        if(i*dt==enz_t){
            sys.set_alphaR(new_alphaR); // Simulate adding the enzyme
        }
        // Solve for next time step using given method:
        mysolver.advance(i,dt,method);
    }
}
