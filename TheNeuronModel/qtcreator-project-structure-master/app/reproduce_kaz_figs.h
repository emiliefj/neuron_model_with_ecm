#ifndef REPRODUCE_KAZ_FIGS_H
#define REPRODUCE_KAZ_FIGS_H

#include"filewriter.h"
#include"kazantsev.h"
#include"integrate_fire_kazantsev.h"
#include"solver.h"

#include <stdlib.h>
#include <iostream>

void reproduce_fig1A(double b);     // A function for reproducing figure 1A in Kazantsev et al. 2012
void investigate_interspike_times(double f_in);
void reproduce_fig2B(double step);  // A function for reproducing figure 2B in Kazantsev et al. 2012
void reproduce_fig2B_IF(double step);
void reproduce_fig5(string method, double step); // A function for reproducing figure 5 in Kazantsev et al. 2012
//void measure_spike_length(double V, double dt);



#endif // REPRODUCE_KAZ_FIGS_H

