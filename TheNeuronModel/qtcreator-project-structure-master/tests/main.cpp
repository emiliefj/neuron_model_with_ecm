#include <unittest++/UnitTest++.h>

#include <kazantsev.h>
#include <hodgkinhuxley.h>
#include <kazantsev_pulse.h>
#include <solver.h>

#include <iostream>
#include <stdlib.h>
#include <vector>

using namespace std;

TEST(solverFunctions){

    double dt= 0.05;
    int t = 153;

    kazantsev testsys;
    testsys.set_initial_values(dt);
    solver testsolver(&testsys);

    // Set values:
    testsys.V = 30.2;
    testsys.Cm = 1.2;
    testsys.non_gating_variables[1].x = 2.8;

    testsys.set_gammaZ(-0.7);
    testsys.set_Ith(6.2);
    testsys.set_conductances(5.5,-9,12.1);
    testsys.set_equilibrium_potentials(17,-5,55.6);
    testsys.set_gating_variable_values(-0.75,1.3,0.9);


    // Testing explicit_forward_euler(dt,t):
    CHECK_CLOSE(-27.5653125,testsys.I_Na(),1e-5);
    CHECK_CLOSE(-904.81248,testsys.I_K(),1e-5);
    CHECK_CLOSE(-307.34,testsys.I_l(),1e-5);
    CHECK_CLOSE(-1239.717793,testsys.Imem(),1e-5);
    CHECK_CLOSE(-5.952,testsys.Ith(),1e-5);
    testsys.set_seed(32);
    testsolver.explicit_forward_euler(dt,t);
    CHECK_CLOSE(82.3152676,testsys.V,1e-5);

    // Check that the solvers give the same result
    int T = 3000;//50000;
    dt= 0.0001;

    // FE:
    kazantsev_pulse fe;
    fe.set_initial_values(dt);
    solver fesolver(&fe);
    vector<double> fe_vec(T);

    // RL:
    kazantsev_pulse rl;
    rl.set_initial_values(dt);
    solver rlsolver(&rl);
    vector<double> rl_vec(T);

    // RK4:
    kazantsev_pulse rk;
    rk.set_initial_values(dt);
    solver rksolver(&rk);
    vector<double> rk_vec(T);
    vector<double> test_vec(T);


    for(int i=0;i<T;i++){
        fe_vec[i] = fe.V;
        fesolver.advance(i,dt,"fe");
        rl_vec[i] = rl.V;
        rlsolver.advance(i,dt,"rl");
        rk_vec[i] = rk.V;
        rksolver.advance(i,dt,"rk4");
        test_vec[i] = fe_vec[i]-rk_vec[i];
    }

    //CHECK_ARRAY_CLOSE(fe_vec, rk_vec, T, 1e-1);
    CHECK_ARRAY_CLOSE(fe_vec, rl_vec, T, 1e-1);
}

TEST(KazantsevPulseMemberFunctions){
    kazantsev_pulse kp_sys;

    kp_sys.set_Isyn_amplitude(33);
    kp_sys.set_Isyn_time(2,5);
    double sum;
    double val;

    for(int t=0;t<20;t++){
        sum += kp_sys.Isyn(t,0.5);
    }

    CHECK_CLOSE(232,sum,1e-5);
}


TEST(HodgkinHuxleyMemberFunctions){
    hodgkinhuxley hh;
    hh.set_initial_values(0.2);

    // Test I_Na(), I_K() og I_l();
    hh.set_conductances(-55,8,12.5);
    hh.set_equilibrium_potentials(32,-18.2,-95);
    hh.set_gating_variable_values(0.32,1.2,0.85);
    hh.V=-54;

    CHECK_CLOSE(131.74374,hh.I_Na(),1e-5);
    CHECK_CLOSE(-593.87904,hh.I_K(),1e-5);
    CHECK_CLOSE(512.5,hh.I_l(),1e-5);
    CHECK_CLOSE(50.3647,hh.Imem(),1e5);

    // Test dV_dt(dt,t)
    hh.set_C(3.6);
    CHECK_CLOSE(-13.99019,hh.dV_dt(0.2,3),1e5);
}


TEST(KazantsevMemberFunctions) {
    hodgkinhuxley hh;
    kazantsev kaz;
    kaz.set_initial_values(0.2);

    // Testing kaz.b():
    kaz.set_b(4);
    kaz.set_R(3);
    kaz.set_Z(1.5);
    kaz.set_gammaZR(0.04);

    CHECK_EQUAL(kaz.b(),4.72);

    // Testing kaz.draw_uniform number()
    CHECK(kaz.draw_uniform_number()>0);
    CHECK(kaz.draw_uniform_number()<1);
    kaz.set_seed(5);
    CHECK_EQUAL(kaz.draw_uniform_number(),0.675);

    // Testing kaz.draw_interspike_time()
    kaz.set_seed(14);
    kaz.set_fin(0.55);
    CHECK_CLOSE(kaz.draw_interspike_time(),2.0888,1e-4);

    // Testing kaz.draw_current_amplitude()
    kaz.set_seed(700);
    CHECK_CLOSE(kaz.draw_current_amplitude(),2.2483,1e-4);

    // Testing kaz.Isyn()
    kaz.set_seed(700);
    CHECK_CLOSE(kaz.Isyn(122,0.2),4.40223,1e-4);

    // Testing kaz.Ith():
    kaz.set_Ith(3.2);
    kaz.set_Z(2.8);
    kaz.set_gammaZ(0.12);
    CHECK_CLOSE(kaz.Ith(),4.2752,1e-4);

}

TEST(KazantsevSetandGets) { // Unit tests for the SET functions in kazantsev.cpp
    kazantsev kaz;

    kaz.set_b(2.5);
    CHECK_EQUAL(kaz.b0,2.5);
    kaz.set_Ith(12.2);
    CHECK_EQUAL(kaz.I0,12.2);
    kaz.set_fin(2.3);
    CHECK_EQUAL(kaz.f_in,2.3);
    kaz.set_gammaZR(0.28);
    CHECK_EQUAL(kaz.gamma_ZR,0.28);
    kaz.set_gammaZ(0.25);
    CHECK_EQUAL(kaz.gamma_Z,0.25);

    kaz.set_Q(5.5);
    CHECK_EQUAL(kaz.non_gating_variables[0].x,5.5);
    kaz.set_Z(6.5);
    CHECK_EQUAL(kaz.non_gating_variables[1].x,6.5);
    kaz.set_P(7.5);
    CHECK_EQUAL(kaz.non_gating_variables[2].x,7.5);
    kaz.set_R(8.5);
    CHECK_EQUAL(kaz.non_gating_variables[3].x,8.5);

}


int main()
{

    return UnitTest::RunAllTests();

}

