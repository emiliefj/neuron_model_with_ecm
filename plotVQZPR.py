### Plotting ###

from numpy import *
import matplotlib.pyplot as plt
import matplotlib.lines as mlines


class Plotter(object):
	"""A class for plotting results from the NeuronModel C++ project""" 
	def __init__(self):
		print "Initiated plotter object"
				

	def plot(self,title,ylab,xlab,ymin,ymax,x,y,style='b',grid=False,plotlabel=None,loc=4,show=True):

		plt.ylim(ymin,ymax)
		plt.plot(x,y,style,label=plotlabel,markersize=15,linewidth=4)
		plt.title(title, fontsize=56, y=1.02) # Increasing y moves the title higher
		plt.ylabel(ylab, fontsize=54)
		plt.xlabel(xlab, fontsize=50)
		if plotlabel:
			plt.legend(prop={'size':50}, loc=loc)
		plt.tick_params(axis='x', labelsize=48)
		plt.tick_params(axis='y', labelsize=43)
		if grid:
			plt.grid()
		if show:
			plt.show()

	def plot_V(self,filename="testfil_V_kaz_fe_T_2000_dt_0.050000.txt",title="The membrane potential",ylab=r'V(mV)',xlab=r't(s)',ymin=-85,ymax=50,style='r--',grid=False,loc=1,label='The membrane voltage $V$',show=True):
		self.read_file(filename)
		y, x = self.V, self.t
		self.plot(title,ylab,xlab,ymin,ymax,x,y, style=style,grid=grid, plotlabel=label,loc=loc, show=show)

	def plot_Q(self,filename="check_Q_3min_kaz_fe_T_24000000_dt_0.005000.txt",title=r"The average activity $Q$",ylab=r'Q',xlab=r't(s)',ymin=0,ymax=8,style='r--',grid=False,loc=1,label='$Q$',show=True):
		self.read_file(filename)
		y, x = self.Q, self.t
		self.plot(title,ylab,xlab,ymin,ymax,x,y, style=style,grid=grid, plotlabel=label,loc=loc, show=show)

	def plot_Z(self,title=r"The ECM conzentration $Z$",ylab=r'Z',xlab=r't(s)',ymin=-1,ymax=8,style='r--',grid=False,loc=1,label='$Z$',show=True):
		y, x = self.Z, self.t
		self.plot(title,ylab,xlab,ymin,ymax,x,y, style=style,grid=grid, plotlabel=label,loc=loc,show=show)
		
	def plot_P(self,title="The concentration of protease, $R$",ylab=r'P',xlab=r't(s)',ymin=0,ymax=10,style='r--',grid=False,loc=1,label='$P$',show=True):
		y, x = self.P, self.t
		self.plot(title,ylab,xlab,ymin,ymax,x,y, style=style,grid=grid, plotlabel=label,loc=loc,show=show)

	def plot_R(self,title="The concentration of ECM-receptors, $R$",ylab=r'R',xlab=r't(s)',ymin=0,ymax=8,style='r--',grid=False,loc=1,label='$R$',show=True):
		y, x = self.R, self.t
		self.plot(title,ylab,xlab,ymin,ymax,x,y, style=style,grid=grid, plotlabel=label,loc=loc,show=show)

	def plot_QZPR_together(self):

		plt.figure(1)                # the first figure
		plt.subplot(411)
		plotter.plot_Q(style='k',title="",xlab='',ymin=0.0,ymax=2.0,grid=True,show=False) # Q
		plt.gca().get_xaxis().set_ticklabels([])
		plt.title(r'Steady state values reached for $f_{in}=1.2kHz$', fontsize=24)
		
		plt.subplot(412)
		plotter.plot_R(style='r',title="",xlab='',ymin=1,ymax=2.8,grid=True,show=False) # R
	
		plt.gca().get_xaxis().set_ticklabels([])
		plt.subplot(413)
		plotter.plot_Z(style='b',title="",xlab='',ymin=0.00,ymax=6.5,grid=True,show=False) # Z
	
		plt.gca().get_xaxis().set_ticklabels([])
		plt.subplot(414)
		plotter.plot_P(style='g',title="",ymin=-1,ymax=12.,grid=True,show=False) # P

	def plot_b(self,title="Scaling of the strength of the synaptic input, $b$",ylab=r'b',xlab=r't(s)',ymin=4,ymax=8,style='k',grid=False,loc=1, label=None,show=True):
		y, x = self.b, self.t
		self.plot(title,ylab,xlab,ymin,ymax,x,y, style=style,grid=grid, plotlabel=label,loc=loc, show=show)

	def plot_Ith(self,title="Current regulating the spikethreshold, $I_{th}$",ylab=r'$I_{th}$',xlab=r't(s)',ymin=0,ymax=3,style='r',grid=False,loc=1, label=None,show=True):
		y, x = self.R, self.t
		self.plot(title,ylab,xlab,ymin,ymax,x,y, style=style,grid=grid, plotlabel=label,loc=loc, show=show)


	def plot_Q_of_freq(self,filename="testfil_recfig2B_fin_0kHz_2kHz_kaz_rl_T_2400000_dt_0.050000.txt",title="Q vs $f_{input}$",ylab=r'Q',xlab=r'$f_{input}$(kHz)',ymin=-0,ymax=8,style='r--',grid=False, label='Average $Q$',show=True):

		self.read_Q_file(filename)
		f0 = 0.83012
		A1 = 0.02861
		A2 = 2.80585

		y, x = self.Q, self.f_in
		Q_inf = 100*self.tau*self.f
		Q_theory = A2 + (A1-A2)/(1+(self.f_in/f0)*(self.f_in/f0))

		plt.plot(x,Q_theory,'b',label="theoretical $Q$",linewidth=6)
		self.plot(title,ylab,xlab,ymin,ymax,x,y, style=style,grid=grid, plotlabel=label,loc=2,show=show)


	def plot_Q_of_Ith(self,filename="recfig5_b_10.000000_kHz_Ith_4.000000_16.000000_kaz_fe_T_24000000_dt_0.005000.txt",title="Q vs $I_{th}$",ylab=r'Q',xlab=r'$I_{th}$($\mu A/cm^2$)',ymin=0,ymax=5,style='r--',grid=False, label='Average $Q$',show=True):
			self.read_Ith_Q_file(filename)
			x,y1,y2,y3 = self.Ith,self.Q1,self.Q2,self.Q3

			self.plot(title,ylab,xlab,ymin,ymax,x,y1, style='ro',grid=grid, plotlabel="$f_{in}=0.1$kHz", show=False)
			self.plot(title,ylab,xlab,ymin,ymax,x,y2, style='g^',grid=grid, plotlabel="$f_{in}=0.2$kHz", show=False)
			self.plot(title,ylab,xlab,ymin,ymax,x,y3, style='bs',grid=grid, plotlabel="$f_{in}=0.3$kHz", show=True)


	def read_hist_file(self,fn):
		with open(fn,'r') as infile:
			self.comment = infile.readline()
			self.N = int(infile.readline())		# number of datapoints
			self.b = float(infile.readline())	# value for b
			self.A = zeros(self.N)
			i = 0;
			for line in infile:
				values = line.split()
				self.A[i] = float(values[0])
				i += 1

	def plot_EPSC_hist(self,filename="EPSCamplitudes_N_100000_b_6.000000___T_100000_dt_6.000000.txt",title="",nbins=50,ylab=r'Probability',xlab=r'$I_{EPSC}$',ymin=-0,ymax=0.15,style='r--',hist_color='b',grid=False, label='P($I_{EPSC}$)',show=True):
		self.read_hist_file(filename)

		b = self.b
		Imax = 40
		x = linspace(0,Imax,Imax)
		x = arange(0,Imax,0.1)
		P = 2*x/(b*b)*exp(-x*x/(b*b))

		plt.hist(sort(self.A),bins=nbins,normed=1,color=hist_color)
		plt.ylim(ymin,ymax)
		plt.xlim(0,25)
		plt.plot(x,P,style,label=label,linewidth=6)
		plt.title(title, fontsize=56)
		plt.ylabel(ylab, fontsize=58)
		plt.xlabel(xlab, fontsize=58)
		if label:
			plt.legend(prop={'size':55}, loc=1)
		plt.tick_params(axis='x', labelsize=48)
		plt.tick_params(axis='y', labelsize=48)
		if grid:
			plt.grid()
		if show:
			plt.show()


	def plot_interspike_times(self, filename="interspiketimes_fin_0.200000___T_100000_dt_0.200000.txt",title="The time t between spikes for $f_{input}=0.2$kHz",nbins=50,ylab="Probability, P(t)",xlab="time(ms)",ymin=0,ymax=1,style='r--',hist_color='b',grid=False, label='P($t_{I_{EPSC}}$)',show=True):
		self.read_spiketime_file(filename)
		x = sort(self.isp) # array of interspike times
		Imax = 40
		r = self.f_in # input frequency, kHz
		t = arange(0,Imax,0.1)
		P = r*exp(-r*t) # Theoretical distribution

		plt.hist(x,bins=nbins,normed=1,color=hist_color)
		plt.plot(t,P,style,linewidth=6, label=label)
		plt.title(title, fontsize=58, y=1.02)
		plt.ylabel(ylab, fontsize=54)
		plt.xlabel(xlab, fontsize=54)
		if label:
			plt.legend(prop={'size':60}, loc=1)
		plt.tick_params(axis='x', labelsize=45)
		plt.tick_params(axis='y', labelsize=45)
		if grid:
			plt.grid()
		if show:
			plt.show()


	def read_file(self,filename): # TODO: generalize
		self.filename = filename
		with open(self.filename,'rb') as infile:
			self.comment = infile.readline()
			self.nr_of_timesteps = int(infile.readline())+1
			self.dt = float(infile.readline())/1000 # from ms to s
			self.names = infile.readline().split()
			self.V = zeros(self.nr_of_timesteps)
			self.Q = zeros(self.nr_of_timesteps)
			self.Z = zeros(self.nr_of_timesteps)
			self.P = zeros(self.nr_of_timesteps)
			self.R = zeros(self.nr_of_timesteps)
			self.b = zeros(self.nr_of_timesteps)
			self.Ith = zeros(self.nr_of_timesteps)
			self.t = zeros(self.nr_of_timesteps)
			i = 0;
			for line in infile:
				values = line.split()
				self.V[i] = float(values[0])
				self.Q[i] = float(values[1])
				self.Z[i] = float(values[2])
				self.P[i] = float(values[3])
				self.R[i] = float(values[4])
				self.b[i] = float(values[5])
				self.Ith[i] = float(values[6])
				self.t[i] = float(values[7])*self.dt
				i += 1

	def read_Ith_Q_file(self,fn):
		Imax = 16.0
		Imin = 4.0
		with open(fn,'r') as infile:
			self.comment = infile.readline()
			infile.readline() # nr of timesteps
			self.Istep = float(infile.readline())
			number_of_steps = (Imax-Imin)/self.Istep+1
			self.Ith = linspace(Imin,Imax,number_of_steps)
			infile.readline()
			self.names = infile.readline().split()
			self.Q1 = zeros(number_of_steps)
			self.Q2 = zeros(number_of_steps)
			self.Q3 = zeros(number_of_steps)
			i = 0;
			for line in infile:
				values = line.split()
				if(i<number_of_steps):
					self.Q1[i] = float(values[1])
				elif(i<2*number_of_steps):
					self.Q2[i-number_of_steps] = float(values[1])
				else:
					self.Q3[i-2*number_of_steps] = float(values[1])
				i += 1
			

	def read_Q_file(self, fn):
		with open(fn,'r') as infile:
			self.comment = infile.readline()
			infile.readline() # nr of timesteps
			self.dt = float(infile.readline())
			self.nr_of_freqs = float(infile.readline())+1
			self.names = infile.readline().split()
			self.Q = zeros(self.nr_of_freqs)
			self.f_in = zeros(self.nr_of_freqs)
			self.f = zeros(self.nr_of_freqs)
			self.tau = zeros(self.nr_of_freqs)
			i = 0;
			for line in infile:
				values = line.split()
				self.f_in[i] = float(values[0])
				self.Q[i] = float(values[1])
				i += 1

	def read_spiketime_file(self,fn):
		with open(fn,'r') as infile:
			self.comment = infile.readline()
			nr_of_timesteps = int(infile.readline()) # nr of timesteps
			self.f_in = float(infile.readline()) # from ms to s
			infile.readline() # nr of timesteps again :P
			self.isp = zeros(nr_of_timesteps)
			i = 0;
			for line in infile:
				self.isp[i] = float(line)
				i += 1

	def normalize_crosscor(self):
		Q, R, Z, P = self.Q, self.R, self.Z, self.P

		Qa = (Q - mean(Q)) / (std(Q) * len(Q))
		Qv = (Q - mean(Q)) /  std(Q)

		Ra = (R - mean(R)) / (std(R) * len(R))
		Rv = (R - mean(R)) /  std(R)

		Za = (Z - mean(Z)) / (std(Z) * len(Z))
		Zv = (Z - mean(Z)) /  std(Z)

		Pa = (P - mean(P)) / (std(P) * len(P))
		Pv = (P - mean(P)) /  std(P)

		self.Qa,self.Qv,self.Ra,self.Rv,self.Za,self.Zv,self.Pa,self.Pv = Qa,Qv,Ra,Rv,Za,Zv,Pa,Pv


	def calculate_cross_correlation(self):
		
		self.normalize_crosscor()

		Q, R, Z, P = self.Q, self.R, self.Z, self.P
		Qa,Qv,Ra,Rv,Za,Zv,Pa,Pv = self.Qa,self.Qv,self.Ra,self.Rv,self.Za,self.Zv,self.Pa,self.Pv

		QQ = correlate(Qa,Qv,mode='same')
		#QQ = QQ*1.0/sum(QQ)
		QR = correlate(Qa,Rv,mode='same')
		#QR = QR*1.0/sum(QR)
		QZ = correlate(Qa,Zv,mode='same')
		#QZ = QZ*1.0/sum(QZ)
		QP = correlate(Qa,Pv,mode='same')
		#QP = QP*1.0/sum(QP)

		RQ = correlate(Ra,Qv,mode='same')
		RR = correlate(Ra,Rv,mode='same')
		RZ = correlate(Ra,Zv,mode='same')
		RP = correlate(Ra,Pv,mode='same')

		ZQ = correlate(Za,Qv,mode='same')
		ZR = correlate(Za,Rv,mode='same')
		ZZ = correlate(Za,Zv,mode='same')
		ZP = correlate(Za,Pv,mode='same')

		PQ = correlate(Pa,Qv,mode='same')
		PR = correlate(Pa,Rv,mode='same')
		PZ = correlate(Pa,Zv,mode='same')
		PP = correlate(Pa,Pv,mode='same')

		self.QQ,self.QR,self.QZ,self.QP = QQ,QR,QZ,QP
		self.RQ,self.RR,self.RZ,self.RP = RQ,RR,RZ,RP
		self.ZQ,self.ZR,self.ZZ,self.ZP = ZQ,ZR,ZZ,ZP
		self.PQ,self.PR,self.PZ,self.PP = PQ,PR,PZ,PP


	def plot_cross_correlation(self):
		
		QQ, QR, QZ, QP = self.QQ, self.QR, self.QZ, self.QP
		RQ, RR, RZ, RP = self.RQ, self.RR, self.RZ, self.RP
		ZQ, ZR, ZZ, ZP = self.ZQ, self.ZR, self.ZZ, self.ZP
		PQ, PR, PZ, PP = self.PQ, self.PR, self.PZ, self.PP
		tau = arange(-size(QR)/2,size(QR)/2)
		tau=tau*1.0/1000


		plt.figure(1)
		plt.subplot(4,4,1)
		self.plot("Q","Q",'',-0.8,1.2,tau,QQ,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])

		plt.subplot(4,4,2)
		self.plot("R",'','',-0.8,1.2,tau,QR,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])
		plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,3)
		self.plot("Z",'','',-0.8,1.2,tau,QZ,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])
		plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,4)
		self.plot("P",'','',-0.8,1.2,tau,QP,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])
		plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,5)
		self.plot('','R','',-0.8,1.2,tau,RQ,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])
		#plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,6)
		self.plot('','','',-0.8,1.2,tau,RR,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])
		plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,7)
		self.plot('','','',-0.8,1.2,tau,RZ,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])
		plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,8)
		self.plot('','','',-0.8,1.2,tau,RP,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])
		plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,9)
		self.plot('',"Z",'',-0.8,1.2,tau,ZQ,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])

		plt.subplot(4,4,10)
		self.plot('','','',-0.8,1.2,tau,ZR,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])
		plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,11)
		self.plot('','','',-0.8,1.2,tau,ZZ,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])
		plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,12)
		self.plot('','','',-0.8,1.2,tau,ZP,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels([])
		plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,13)
		self.plot('','P','t(s)',-0.8,1.2,tau,PQ,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels(['','-40','','0','','40',''])
		#plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,14)
		self.plot('','','t(s)',-0.8,1.2,tau,PR,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels(['','-40','','0','','40',''])
		plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,15)
		self.plot('','','t(s)',-0.8,1.2,tau,PZ,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels(['','-40','','0','','40',''])
		plt.gca().get_yaxis().set_ticklabels([])

		plt.subplot(4,4,16)
		self.plot('','','t(s)',-0.8,1.2,tau,PP,style='b',grid=True,plotlabel=None,show=False)
		plt.gca().get_xaxis().set_ticklabels(['','-40','','0','','40',''])
		plt.gca().get_yaxis().set_ticklabels([])

	def plot_power_spectrum(self):
		Q = self.P
		dt = self.dt

		ts = 0.02
		Q = Q-mean(Q)
		print Q
		ps = abs(fft.fft(Q))**2
		freqs = fft.fftfreq(Q.size, ts)
		idx = argsort(freqs)

		plt.plot(freqs[idx], ps[idx])

	def compare_solvers(self,fn_fe="running_kaz_pulse_kaz_fe_T_10000_dt_0.005000.txt",fn_rk4="running_kaz_pulse_kaz_rk4_T_10000_dt_0.005000.txt",fn_rl="running_kaz_pulse_kaz_rl_T_10000_dt_0.005000.txt"):
		filename_fe = fn_fe
		filename_rk4 = fn_rk4
		filename_rl = fn_rl
		V_title = r'The membrane potential'# for $b=6$, $f_{in}=0.2kHz$, $I_{th}=4.5\mu A/cm^2$'
	
		## Plotting the membrane potential calculated with the different solvers 
		self.plot_V(filename=filename_fe,title=V_title,style="y",grid=True,label="FE",show=False)
		Vfe = self.V
		self.plot_V(filename=filename_rk4,title=V_title,style="g--",grid=True,label="RK4",show=False)
		Vrk4 = self.V
		self.plot_V(filename=filename_rl,title=V_title,style="b-.",grid=True,label="RL",show=True)
		Vrl = self.V
	
		# Plotting the error:
		V_fe_rk4 = Vfe-Vrk4
		V_rl_fe = Vrl-Vfe
		V_rl_rk4 = Vrl-Vrk4

		E_fe_rk4 = sqrt(V_fe_rk4**2)
		E_rl_fe = sqrt(V_rl_fe**2)
		E_rl_rk4 = sqrt(V_rl_rk4**2)

		ymin = -0.01
		ymax = 0.8
		self.plot(title="Error",ylab="Error",xlab="t",ymin=ymin,ymax=ymax,x=self.t,y=E_fe_rk4,style='b',grid=False,plotlabel="FE-RK4",loc=1,show=False)
		self.plot(title="Error",ylab="Error",xlab="t",ymin=ymin,ymax=ymax,x=self.t,y=E_rl_rk4,style='m',grid=False,plotlabel="RL-RK4",loc=1,show=False)
		self.plot(title="Error",ylab="Error",xlab="t",ymin=ymin,ymax=ymax,x=self.t,y=E_rl_fe,style='g',grid=True,plotlabel="RL-FE",loc=1,show=True)


if __name__ == '__main__':

	


	##############################
	## Reproduce fig 5 in Kaz:  ##
	##############################
	#filename = "recfig5_b_10.000000_kHz_Ith_4.000000_16.000000_kaz_fe_T_24000000_dt_0.050000.txt"
	#plotter= Plotter()
	#plotter.plot_Q_of_Ith(filename=filename)


	#########################################
	## Plot histogram of interspike times: ##
	#########################################
	#plotter= Plotter()
	#plotter.plot_interspike_times(filename="interspiketimes_fin_0.200000___T_100000000_dt_0.200000.txt",nbins=100)

	##############################
	## Reproduce fig 1A in Kaz: ##
	##############################
	#plotter= Plotter()
	#plotter.plot_EPSC_hist(filename="EPSCamplitudes_N_100000000_b_10.000000___T_100000000_dt_10.000000.txt",nbins=50,ymax=0.10,style='r--',label="$b=10$",show=True)
	#plotter.plot_EPSC_hist(filename="EPSCamplitudes_N_100000000_b_10.000000___T_100000000_dt_10.000000.txt",nbins=100,style='go',hist_color='m',label="$b=10$")


	#######################
	## Plot Q over time: ##
	#######################
	#filename1="running_f_in_2.000000_b_6.000000_kaz_fe_T_100000000_dt_0.000100.txt"
	#filename2 = "running_f_in_2.000000_b_6.000000_kaz_rk4_T_100000000_dt_0.000100.txt"
	#plotter= Plotter()
	#plotter.plot_Q(filename=filename1,style='r',label="fe",loc=4, grid=True,show=False)
	#plotter.plot_Q(title="The average activity, $Q$",filename=filename2,ymin=0,ymax=6,style='b',label="rk4",loc=4,grid=True)


	##############################
	## Reproduce fig 2B in Kaz: ##
	##############################
	#plotter = Plotter()
	#filename = "recfig2B_b_6.000000_Ith_4.500000_kaz_fe_T_240000000_dt_0.000500.txt"
	#plotter.plot_Q_of_freq(filename=filename,title="Q vs $f_{input}$",ylab=r'Q',xlab=r'$f_{input}$(kHz)',ymin=0,ymax=8,style='ro',grid=True, label='measured $Q$',show=True)


	####################
	## Plot V normal: ##
	####################
	#filename = "running_integrate_fire_kaz_fe_T_1000000_dt_0.001000.txt"
	#V_title = r'The membrane potential'# for $b=6$, $f_{in}=0.2kHz$, $I_{th}=4.5\mu A/cm^2$'
	#plotter = Plotter()
	#plotter.plot_V(filename=filename,title=V_title,style="b-.",grid=True,label=None,show=True)


	######################
	## Compare solvers: ##
	######################

	#filename_fe = "running_kaz_pulse_kaz_fe_T_500000_dt_0.000100.txt"
	#filename_rk4 = "running_kaz_pulse_kaz_rk4_T_500000_dt_0.000100.txt"
	#filename_rl = "running_kaz_pulse_kaz_rl_T_500000_dt_0.000100.txt"

	### V1 ###
	#V_title = r'The membrane potential'# for $b=6$, $f_{in}=0.2kHz$, $I_{th}=4.5\mu A/cm^2$'
	#plotter = Plotter()
	#plotter.plot_V(filename=filename_fe,title=V_title,style="y",grid=True,label="FE",show=False)
	#plotter.plot_V(filename=filename_rk4,title=V_title,style="g--",grid=True,label="RK4",show=False)
	#plotter.plot_V(filename=filename_rl,title=V_title,style="b-.",grid=True,label="RL",show=True)

	### V2 ###
	#plotter = Plotter()
	#plotter.compare_solvers(fn_fe=filename_fe,fn_rk4=filename_rk4,fn_rl=filename_rl)

	#####################################
	## Reproduce fig 3.11a in Sterrat: ##
	#####################################
	#V_title = r'The membrane potential for varying initial depolarizations'
	#plotter.plot_V(filename="testfil_box3.5_64_plus_90_hh_rl_T_1200_dt_0.005000.txt",title=V_title,label="$V_0=-64+90$",style="r",show=False)
	#plotter.plot_V(filename="testfil_box3.5_64_plus_15_hh_l_T_1200_dt_0.005000.txt",title=V_title,style="b",label="$V_0=-64+15$",show=False)
	#plotter.plot_V(filename="testfil_box3.5_64_plus_7_hh_rl_T_1200_dt_0.005000.txt",title=V_title,style="g",label="$V_0=-64+7$",show=False)
	#plotter.plot_V(filename="testfil_box3.5_64_plus_6_hh_rl_T_1200_dt_0.005000.txt",title=V_title,style="y",label="$V_0=-64+6$",show=False)
	#plt.show();

	##########################################################
	## Plot the variables related to the ECM-concentration: ##
	##########################################################
	#filename_low = "running_low_activity_0.500000_kaz_rl_T_120000000_dt_0.001000.txt"
	#filename = "running_f_in_1.400000_b_6.000000_kaz_fe_T_100000000_dt_0.000100.txt"#"running_mid_activity_2.000000_kaz_rl_T_360000000_dt_0.000500.txt"
	#plotter = Plotter()
	#plotter.read_file(filename)
	#plt.figure(1)                # the first figure
	#plt.subplot(411)
	#plotter.plot_Q(filename=filename,style='k',title="",xlab='',ymin=0,ymax=10,loc=2,grid=True,show=False) # Q
	#plt.gca().get_xaxis().set_ticklabels([])
	##plt.title(r'Kazantsev with a strong pulse current added after 5 sec', fontsize=44)
	##plt.title(r'Steady state values reached for IF neuron, $I=1.544\ \mu$A/cm$^2$', fontsize=44)
	#plt.title(r'The Kazantsev model', fontsize=44)
	##plt.title(r'$\alpha_Z$ increased from 1.0 to 5.0 after 60s', fontsize=44)
#
	#plt.subplot(412)
	#plotter.plot_R(style='r',title="",xlab='',ymin=0.5,ymax=2.5,loc=4,grid=True,show=False) # R
#
	#plt.gca().get_xaxis().set_ticklabels([])
	#plt.subplot(413)
	#plotter.plot_Z(style='b',title="",xlab='',ymin=0,ymax=10,loc=1,grid=True,show=False) # Z
#
	#plt.gca().get_xaxis().set_ticklabels([])
	#plt.subplot(414)
	#plotter.plot_P(style='g',title="",ymin=-1,ymax=12,grid=True,loc=1,show=False) # P
	
	#plt.figure(2)
	#plotter.plot_b(ymin=5.5,ymax=15,title=r"Scaling factor $b$ for $f_{in}=1.5kHz$ and $\gamma_{ZR} = 0.02$")
#
	#plt.figure(3)
	#plotter.plot_Ith(ymin=0,ymax=2.2,title=r'Threshold current $I_{th}$ for $f_{in}=1.5kHz$ and $\gamma_{Z} = 0.0345$')


	#####################################
	## Plot the correlation functions: ##
	#####################################
	#plotter.calculate_cross_correlation()
	#plotter.plot_cross_correlation()


	#plotter.plot_P(title="The concentration of ECM-molecules",ylab="Q,Z,R,P", style='g',show=False)
	#plt.show()

	# Plot Q vs f_in (repr fig 2B)
	#avgQ_fn = "testfil_recfig2B_fin_0kHz_2kHz_fe_b_6.0_kaz_fe_T_24000000_dt_0.005000.txt"
	#Q_title = r'The average activity $Q$, $b=6$, $f_{in}=1.8kHz$, $I_{th}=4.5\mu A/cm^2$'
	#Qvsf_title = r'The average activity $Q$ vs $f_{input}$ for $b=6$, $I_{th}=4.5\mu A/cm^2$'
	#QvsI_title = r'The average activity $Q$ vs $I_{th}$ for $b=10$'#, $f_{input}=0.1kHz$'
	##plotter.plot_Q(title=r"The average activity $Q$, $f_{input}=0.2$kHz, $I_{th}=8.5\mu A/cm^2$")
	#plotter.plot_Q_of_freq(filename=avgQ_fn,title=Qvsf_title)

	#plotter.plot_Q_of_Ith(filename="testfil_recfig5_fin_0.100000kHz_Ith_4.000000_14.000000_kaz_fe_T_48000000_dt_0.005000.txt",title=QvsI_title, style='ro', label='$f_{input}=0.1$kHz')
	#plotter.plot_Q_of_Ith(filename="testfil_recfig5_fin_0_2kHz_Ith_4.000000_14.000000_kaz_fe_T_48000000_dt_0.005000.txt",title=QvsI_title, style='g^', label='$f_{input}=0.2$kHz')
	#plotter.plot_Q_of_Ith(filename="testfil_recfig5_fin_0.300000kHz_Ith_4.000000_14.000000_kaz_fe_T_48000000_dt_0.005000.txt",title=QvsI_title, style='bs', label='$f_{input}=0.3$kHz')
	
	#plotter.plot_interspike_times()

	#plotter.plot_power_spectrum()

	#plt.show()
	
