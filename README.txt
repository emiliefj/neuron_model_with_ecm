%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%    Author:	Emilie Fjørner			        %%%%
%%%    Contact:	e.s.fjorner@fys.uio 		    %%%%
%%%    										    %%%%
%%%    Date:	16.08.2015					    %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



*** ABOUT THE REPOSITORY ***

This repository holds my numerical model which allows for modeling the electrical activity of a neuron, with or without the influence of a surrounding extracellular matrix (ECM). 
The code is part of my master thesis.

_TECHNICAL DETAILS_
The model is built and run using Qt Creator version 3.0.1, with unit testing implemented using UnitTest++ in version v1.4.

_CONTENTS_
README.txt									% This README
TheNeuronModel								% Folder containing all the model files
plotVQZPR.py								% A python script that can be used to plot the results when running the model
A_Homeostatic_Model_of_Neuronal_Firing.pdf	% The article that introduces the Kazantsev model implemented in the model
plot_activation_functions.p 				% A python script that plots the activation functions introduced in the paper above.
example 									% A folder showing some example runs



*** STRUCTURE OF THE NEURON MODEL ***

The folder TheNeuronModel contains the model system in the way it was created using Qt Creator. Inside it we find two folders: build-MyProject-Desktop-Debug and qtcreator-project-structure-master. The first one, build-MyProject-Desktop-Debug, is of less interest as it only contains all .o files. This is also where output files end up when running the model with Qt Creator.

The other folder qtcreator-project-structure-master contains the actial model implementation:

_CONTENTS OF qtcreator-project-structure-master_
MyProject.pro 							% The .pro file created by Qt Creator
app							% Folder containing applications/methods that run the model
	main.cpp							% Standard run methods. Allows you to run the Hodgkin-Huxley, the Kazantsev model ++
	special_solver_methods.cpp/.hp		% More specific solver methods used in my masters degree. 
	reproduce_kaz_figs.cpp/.h 			% Solver methods specifically for reproducing results from the paper by Kazantsev et al.
scr 						% The source files 
	neuron_system.cpp/.h 				% A virtual class acting as an interface for implementig a neuron model
	hodgkinhuxley.cpp/.h 				% The Hodgkin-Huxley model. Implements neuron_system 
	kazantsev.cpp/.h 					% The Kazantsev model. 
	kazantsev_pulse,cpp/.h 				% THe Kazantsev model with synaptic input in the form of a pulse
	integrate_fire_kazantsev.cpp/.h 	% The Kazantsev model, but with an integrate-and-fire neuron in place of the HH model
	integrate_fire_kaz_synaptic.cpp/.h 	% Same as above except the synaptic input is in the form of a Poisson train
	solver.cpp/.h 						% The solver class. Contains three numerical schemes for solving the model system
	gating_variable.cpp/.h 				% For holding gating variables used in the models
	non_gating_variable.cpp/.h 			% For holding non gating variables used in the models
	filewriter.cpp/.h 					% A helper class for writing to file
tests
	main.cpp 							% Contains various unit tests for testing the implementation



*** ABOUT THE PROJECT ***

Simply put the project contains my implementation of a model for modeling the electrical activity  in a neuron. The project is mainly made up of various model implentaions and a solver for solving these in time. The Solver-class contains three numerical schemes Runge-Kutta4, Forward Euler and Rush-Larsen and these allow you to solve a neuron model that implements the virtual class neuron_model. So far the standard Hodgkin-Huxley model and the Kazantsev model from the paper given in the file A_Homeostatic_Model_of_Neuronal_Firing.pdf have been included, as well as several variations of the Kazantsev model. It should be fairly straightforward to implement further models if so desired. If the model contains the membrane potential V as well as a set of gating and non gating variables to be progressed in time, it can be implemented as a daughter class of the neuron_model class, and the methods in the solver class can be used to solve it. It may be necessary to implement an adapted class for the non-gating variables, depending on the specific details of the model. 


*** HOW TO USE ***

The code is generously commented, and it should be fairly self explanatory to run the various models. It is run from the main()-method in the app folder. Here we can for instance write: 

solve_kazantsev(0.0005,10000,1000,"fe");

if we want to solve the Kazantsev model with dt=0.0005 ms for 10 000 ms with forward euler as the chosen solver scheme, and writing to file every 1000 timestep. The name of the output file will be given in the terminal and generally be on the form:

----
Here is a comment
(number of timesteps written to file)
(value of dt)
     V          Q         Z         P         R         b        Ith        t    
-65.000000  0.000000  0.000000  0.000000  0.000000  6.000000  4.500000  0.000000
.
.
.
-57.725022  5.582127  0.011661  0.000000  2.000000  6.002799  4.501810  34256850.000000
----

Some of the methods may however give differing output files, namely those that are aimed at reproducing the results from the Kazantsev paper.

You can plot the results however you like, but for convenience I have added my plotting script in the file plotVQZPR.py. 

If you have any questions you can contact me using the e-mail given at the top of this document.


*** REPRODUCTION/EXAMPLE PLOTS ***

For reproduction/example purposes I have added a few output files from running the model, incuded in the example folder. The contents of this folder are as follows:


_ HH_dt_0.0001_t_50_fe_rk4_rl_together.png _

A plot of the result when solving the Hodgkin-Huxley model using dt=0.0001ms, t=50ms. The plot compares the three solver schemes. The output files used when creating the plot are as follows:
- solving_HH_fe_T_500000_dt_0.000100.txt -
- solving_HH_rk4_T_500000_dt_0.000100.txt -
- solving_HH_rl_T_500000_dt_0.000100.txt -

The parameters used for the HH model are as given in the file - HH_params.txt - (The contents of this file can be pasted into the set_initial_values()-method of hodgkin_huxley.cpp, for simplicity)


_ kaz_dt_0.0002_t_60000_rl_Q.png _

A plot showing Q as a function of time when running the Kazantsev model solving with the Rush-Larsen scheme using dt=0.0002ms and solving for t=60s. The parameters used for the HH model are as given in the file - kaz_params.txt - (The contents of this file can be pasted into the set_initial_values()-method of kazantsev.cpp, for simplicity).

The output file from the run of the model used to create this plot is given in the file: - running_f_in_2.000000_b_6.000000_kaz_rl_T_300000000_dt_0.000200.txt - 

