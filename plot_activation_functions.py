from numpy import *
import matplotlib.pyplot as plt


def H(Q,x0,x1,theta,k):
	return x0 - (x0-x1)/(1+exp(-(Q-theta)/k))

def Hz(Q):
	z0 = 0
	z1 = 1
	thetaz = 6.5
	kz = 0.15

	return H(Q,z0,z1,thetaz,kz)

def Hp(Q):
	p0 = 0
	p1 = 1
	thetap = 7
	kp = 0.05

	return H(Q,p0,p1,thetap,kp)

def Hr(Q):
	r0 = 2
	r1 = 1
	thetar = 7.3
	kr = 0.1

	return H(Q,r0,r1,thetar,kr)


def plot(x,y,ymin=-0.5,ymax=2.5,title="",xlab='Q',ylab=r'$H_x$',plotlabel=r'$H_z$',style='b',loc=1,grid=False,show=True):
	plt.ylim(ymin,ymax)
	plt.plot(x,y,style,label=plotlabel,markersize=15,linewidth=4)
	plt.title(title, fontsize=56)
	plt.ylabel(ylab, fontsize=54)
	plt.xlabel(xlab, fontsize=54)
	if plotlabel:
		plt.legend(prop={'size':50}, loc=loc)
	plt.tick_params(axis='x', labelsize=48)
	plt.tick_params(axis='y', labelsize=48)
	if grid:
		plt.grid()
	if show:
		plt.show()

def plot_activation_functions(Q):
	plot(Q,Hz(Q),plotlabel=r'$H_z$',show=False)
	plot(Q,Hp(Q),plotlabel=r'$H_p$',style='r',show=False)
	plot(Q,Hr(Q),plotlabel=r'$H_r$',style='g',show=True)


if __name__ == '__main__':
	Q = arange(5.5,8.5,0.01)
	plot_activation_functions(Q)



